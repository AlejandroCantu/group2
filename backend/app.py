from flask import request
from init import app
from models import *
import math
from Facility import *
from County import *
from Companies import *


@app.route("/")
def hello_world():
    return '<img src="https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg" alt="cowboy" />'


@app.route("/companies", methods=["GET"])
def getCompanies():
    page = 1
    per_page = 12

    if "page" in request.args:
        try:
            page = int(request.args["page"])
        except:
            return "Page number invalid"

    if "per_page" in request.args:
        try:
            per_page = int(request.args["per_page"])
        except:
            return "Page size invalid"

    comp_query = db.session.query(Company).filter(
        Company.emissions != None, Company.website != None, Company.address != None
    )

    print("Request")

    search = get_args(request, "q")
    comp_query = search_companies(search, comp_query)
    sort = get_args(request, "sort")
    desc = get_args(request, "descending")
    comp_query = sort_companies(sort, desc, comp_query)
    filter = get_args(request, "state")
    comp_query = filter_companies(filter, "state", comp_query)
    filter = get_args(request, "zip")
    comp_query = filter_companies(filter, "zip", comp_query)
    filter = get_args(request, "city")
    comp_query = filter_companies(filter, "city", comp_query)

    comp_count = comp_query.count()

    comp_page = comp_query.paginate(page, per_page)
    c_list = []
    for comp in comp_page.items:
        c_list.append(comp.as_dict())

    start = (page - 1) * per_page + 1
    return {
        "page": page,
        "per_page": per_page,
        "numInstances": comp_count,
        "list": c_list,
        "pageCnt": math.ceil(comp_count / per_page),
        "startIdx": start,
        "endIdx": start + per_page - 1,
    }


def get_args(request, arg):
    if arg in request.args:
        return request.args[arg]
    else:
        return None


@app.route("/companies/id=<id>", methods=["GET"])
def getCompany_id(id):
    company = db.session.query(Company).filter_by(id=id)
    county = None
    for c in company:
        if c.county_id is not None:
            county = (
                db.session.query(County)
                .filter((County.id) == c.county_id)
                .first()
                .as_dict()
            )

    links = db.session.query(link_companies_fac).filter_by(company_id=id).all()
    facilities = []
    for link in links:
        fac = db.session.query(Facility).filter_by(id=link.facility_id).first()
        facilities.append(fac.as_dict())
    f = {}
    for comp in company:
        c = comp.as_dict()
    c["facilities_list"] = facilities
    c["county"] = county
    return c


@app.route("/counties", methods=["GET"])
def getCounties():
    page = 1
    per_page = 9
    queries = request.args.to_dict()
    if "page" in request.args:
        try:
            page = int(request.args["page"])
        except:
            return "Page number invalid"

    if "per_page" in request.args:
        try:
            per_page = int(request.args["per_page"])
        except:
            return "Page size invalid"

    county_query = db.session.query(County).filter(County.emissions != 0)

    # Search
    county_query = search_counties(queries, county_query)

    # Filtering
    county_query = filter_counties(queries, county_query)

    # Sorting
    county_query = sort_counties(queries, county_query)

    county_count = county_query.count()

    county_page = county_query.paginate(page, per_page)

    c_list = []
    for county in county_page.items:
        officials = (
            db.session.query(Official).filter((Official.county_id) == county.id).count()
        )
        c_list.append({**(county.as_dict()), "numOfficials": officials})

    start = (page - 1) * per_page + 1
    return {
        "page": page,
        "per_page": per_page,
        "numInstances": county_count,
        "list": c_list,
        "pageCnt": math.ceil(county_count / per_page),
        "startIdx": start,
        "endIdx": start + per_page - 1,
    }


@app.route("/counties/id=<id>", methods=["GET"])
def getCounty_id(id):

    county = db.session.query(County).filter_by(id=id)
    officials = db.session.query(Official).filter((Official.county_id) == id).all()
    facilities = db.session.query(Facility).filter((Facility.county_id) == id).all()
    companies = db.session.query(Company).filter((Company.county_id) == id).all()

    o = []
    for official in officials:
        o.append(official.as_dict())

    f = []
    owner_companies = []
    for facility in facilities:
        f.append(facility.as_dict())
        links = (
            db.session.query(link_companies_fac)
            .filter_by(facility_id=facility.id)
            .all()
        )
        for link in links:
            owner = db.session.query(Company).filter_by(id=link.company_id).first()
            owner_companies.append({"name": owner.name, "id": owner.id})

    c = {}
    for cty in county:
        c = cty.as_dict()

    c["numOfficials"] = len(o)
    c["officials"] = o
    c["facilities"] = f
    c["companies"] = owner_companies
    return c


@app.route("/facilities", methods=["GET"])
def getFacilities():
    page = 0
    per_page = 10

    queries = request.args.to_dict()

    if "page" in request.args:
        try:
            page = int(request.args["page"])
        except:
            return "Page number invalid"

    if "per_page" in request.args:
        try:
            per_page = int(request.args["per_page"])
        except:
            return "Page size invalid"

    fac_query = db.session.query(Facility).filter(Facility.parentc != None)

    # search
    fac_query = search_facilities(queries, fac_query)

    # filter
    fac_query = filter_facilities(queries, fac_query)

    # sort
    fac_query = sort_facilities(queries, fac_query)

    fac_count = fac_query.count()

    fac_page = fac_query.paginate(page + 1, per_page)

    f_list = []
    for fac in fac_page.items:
        f_list.append(fac.as_dict())

    start = (page - 1) * per_page + 1
    return {
        "page": page,
        "per_page": per_page,
        "numInstances": fac_count,
        "list": f_list,
        "pageCnt": math.ceil(fac_count / per_page),
        "startIdx": start,
        "endIdx": start + per_page - 1,
    }


@app.route("/facilities/id=<id>", methods=["GET"])
def getFacility_id(id):
    facility = db.session.query(Facility).filter_by(id=id)
    county = None
    for fa in facility:
        if fa.county_id is not None:
            county = (
                db.session.query(County)
                .filter((County.id) == fa.county_id)
                .first()
                .as_dict()
            )

    links = db.session.query(link_companies_fac).filter_by(facility_id=id).all()
    owner_companies = []
    for link in links:
        owner = db.session.query(Company).filter_by(id=link.company_id).first()
        owner_companies.append(owner.as_dict())
    f = {}
    for fac in facility:
        f = fac.as_dict()
    f["companies_list"] = owner_companies
    f["county"] = county
    return f


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
