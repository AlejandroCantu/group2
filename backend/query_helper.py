# Uses code from the TexasVotes project https://gitlab.com/forbesye/fitsbits/-/blob/master/back-end/query_helpers.py
# Wrapper for retrieving keys from dictionary queries
# Returns none if desired key is not in queries
def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None
