from sqlalchemy.orm import Session
import models

# Companies
def get_all_companies(db: Session, city):
    query = db.query(models.Comapny)
    query = query.filter(models.Company.city(city))
    return query.all()


def get_company_by_id(db: Session, company_id):
    company = db.query(models.Company).filter_by(id=company_id).first()
    company = company.__dict__
    return company


# Counties
def get_all_counties(db: Session, state):
    query = db.query(models.County)
    query = query.filter(models.County.state_val(state))
    return query.all()


def get_county_by_id(db: Session, county_id):
    county = db.query(models.County).filter_by(id=county_id).first()
    county = county.__dict__
    return county


# Facilities
def get_all_facilities(db: Session):
    return db.query(models.Facility).all()


def get_facility_by_id(db: Session, facility_id):
    facility = db.query(models.Facility).filter_by(id=facility_id).first()
    facility = facility.__dict__
    return facility
