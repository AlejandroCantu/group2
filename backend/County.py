from sqlalchemy import and_, or_, func
from models import County


# Filters counties by one of the attributes
def filter_county_by(county_query, filters, what):
    if filters == "state":
        county_query = county_query.filter(County.state_val == what)

    elif filters == "income":
        income = what.replace(",", "").replace("$", "").split(" ")
        income_one = income[0]
        income_two = income[2]
        if income_two == "more":
            county_query = county_query.filter(County.income >= int(income_one))
        else:
            county_query = county_query.filter(
                County.income >= int(income_one), County.income <= int(income_two)
            )

    elif filters == "ethnicity":
        county_query = county_query.filter(County.ethnicity == what)

    elif filters == "emissions":
        value_one = what.replace(",", "").split(" ")
        emission_one = value_one[0]
        emission_two = value_one[2]
        if emission_two == "more":
            county_query = county_query.filter(County.emissions >= emission_one)
        else:
            county_query = county_query.filter(
                County.emissions >= emission_one, County.emissions <= emission_two
            )

    return county_query


# Filters counties for all four supported attributes
def filter_counties(queries, county_query):
    state_type = get_query("state", queries)
    income_type = get_query("income", queries)
    ethnicity_type = get_query("ethnicity", queries)
    emission_type = get_query("emissions", queries)

    if state_type != None:
        county_query = filter_county_by(county_query, "state", state_type)

    if income_type != None:
        county_query = filter_county_by(county_query, "income", income_type)

    if ethnicity_type != None:
        county_query = filter_county_by(county_query, "ethnicity", ethnicity_type)

    if emission_type != None:
        county_query = filter_county_by(county_query, "emissions", emission_type)

    return county_query


# Sorts elections by  attributes
# in ascending or descending order
def sort_county_by(sorting, county_query, desc):
    county = None

    try:
        if sorting["sort"] == "emissions":
            county = County.emissions
        elif sorting["sort"] == "name":
            county = County.name
    except KeyError:
        return county_query

    if desc:
        return county_query.order_by(county.desc())
    else:
        return county_query.order_by(county.asc())


# Determines whether attribute will be sorted in ascending or descending order
# Passes attribute to be sorted to sort_election_by for sorting
# Only supports sorting on one attribute at a time
def sort_counties(sort, county_query):
    if sort == None:
        return county_query

    try:
        # descending
        if sort["desc"] == "true":
            return sort_county_by(sort, county_query, True)
        else:
            return sort_county_by(sort, county_query, False)
    except KeyError:
        return sort_county_by(sort, county_query, True)


def search_counties(q, county_query):
    if not q:
        return county_query
    else:
        try:
            terms = q["q"].split()
        except KeyError:
            return county_query

    searches = []

    for term in terms:
        searches.append(County.name.ilike("%{}%".format(term)))
        try:
            searches.append(County.id.match([int(term)]))
            searches.append(County.income.match[int(term)])
        except ValueError:
            pass
        searches.append(func.lower(County.state_val).match(term.lower()))
        searches.append(func.lower(County.ethnicity).match(term.lower()))

    county_query = county_query.filter(or_(*tuple(searches)))

    return county_query


def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None
