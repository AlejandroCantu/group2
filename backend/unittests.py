from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import pytest
from models import Company, db, County, Facility

engine = create_engine(
    "postgresql://FindFootprints:FindThoseFootprints1@findingfootprints-db.cyocgz8iqxu8.us-east-1.rds.amazonaws.com:5432/postgres"
)
session = sessionmaker(bind=engine)


class TestBackend:
    @pytest.mark.run(order=1)

    # Test 1
    def test_get_all_counties(self):
        data = db.session.query(County).filter((County.state_val) == "Texas").all()
        assert isinstance(data, list)
        assert len(data) == 254

    # Test 2
    def test_get_county_by_id(self):
        maricopa_county = {
            "id": 16,
            "name": "Maricopa County",
            "state_val": "Arizona",
            "info": "In 2019, Maricopa County, AZ had a population of 4.49M people with a median age of 36.9 and a median household income of $68,649. Between 2018 and 2019 the population of Maricopa County, AZ grew from 4.41M to 4.49M, a 1.69% increase and its median household income grew from $65,252 to $68,649, a 5.21% increase.",
            "emissions": 4464953,
            "population": 4328810,
            "demographic": "The 5 largest ethnic groups in Maricopa County, AZ are  White (Non-Hispanic) (54.3%), White (Hispanic) (24.7%), Black or African American (Non-Hispanic) (5.53%), Other (Hispanic) (4.59%), and Asian (Non-Hispanic) (4.17%). 26.9% of the households in Maricopa County, AZ speak a non-English language at home as their primary language.",
            "income": 68649,
            "ethnicity": "White (Hispanic)",
        }
        data = db.session.query(County).filter((County.id) == 16).first()

        assert maricopa_county["id"] == data.id
        assert maricopa_county["name"] == data.name
        assert maricopa_county["state_val"] == data.state_val
        assert maricopa_county["info"] == data.info
        assert maricopa_county["emissions"] == data.emissions
        assert maricopa_county["population"] == data.population
        assert maricopa_county["demographic"] == data.demographic
        assert maricopa_county["income"] == data.income
        assert maricopa_county["ethnicity"] == data.ethnicity

    # Test 3
    def test_get_all_companies(self):
        data = db.session.query(Company).filter((Company.city) == "Los Angeles").all()
        assert isinstance(data, list)
        assert len(data) == 5

    # Test 4
    def test_get_company_by_id(self):
        pacificorp_comp = {
            "id": 2,
            "name": "Pacificorp",
            "address": "825 NE MULTNOMAH ST",
            "city": "Portland",
            "state": "OR",
            "zip": "97232",
            "website": "http://pacificorp.com",
            "subsidiaries": 5,
            "branches": 1,
            "founded": "1987",
            "revenue": "5070000000",
            "description": "PacifiCorp is a regulated, vertically integrated electric utility company serving 1.8 million retail customers, including residential, commercial, industrial, irrigation and other customers in portions of the states of Utah, Oregon, Wyoming, Washington, Idaho and California. PacifiCorp owns, or has interests in, 75 thermal, hydroelectric, wind-powered and geothermal generating facilities, with a net owned capacity of 11, 136 megawatts (MW). It owns electric transmission and distribution assets, and transmits electricity through approximately 16, 400 miles of transmission lines and 63, 000 miles of distribution lines and 900 substations. It also buys and sells electricity on the wholesale market with other utilities, energy marketing companies, financial institutions and other market participants. PacifiCorp's subsidiaries support its electric utility operations by providing coal mining services.",
            "linked_in": "https://www.linkedin.com/company/pacificorp",
            "facebook": None,
            "twitter": "pacificorp",
            "phone": "+1 (503) 813-5645",
            "fax": "(801) 220-7649",
            "email": "kristi.olsen@pacificorp.com",
            "county_id": 88,
        }
        data = db.session.query(Company).filter((Company.id) == 2).first()
        assert data
        assert pacificorp_comp["id"] == data.id
        assert pacificorp_comp["name"] == data.name
        assert pacificorp_comp["address"] == data.address
        assert pacificorp_comp["city"] == data.city
        assert pacificorp_comp["state"] == data.state
        assert pacificorp_comp["zip"] == data.zip
        assert pacificorp_comp["website"] == data.website
        assert pacificorp_comp["subsidiaries"] == data.subsidiaries
        assert pacificorp_comp["branches"] == data.branches
        assert pacificorp_comp["founded"] == data.founded
        assert pacificorp_comp["revenue"] == data.revenue
        assert pacificorp_comp["description"] == data.description
        assert pacificorp_comp["linked_in"] == data.linked_in
        assert pacificorp_comp["facebook"] == data.facebook
        assert pacificorp_comp["twitter"] == data.twitter
        assert pacificorp_comp["phone"] == data.phone
        assert pacificorp_comp["fax"] == data.fax
        assert pacificorp_comp["email"] == data.email
        assert pacificorp_comp["county_id"] == data.county_id

    # Test 5
    def test_get_all_facilities(self):
        data = db.session.query(Facility).all()
        assert isinstance(data, list)
        assert len(data) == 267

    # Test 6
    def test_get_facility_by_id(self):
        mariposa_energy = {
            "name": "Mariposa Energy, LLC",
            "address": "4887 Bruns Road",
            "city": "Byron",
            "county_id": 50,
            "emissions": 28,
            "facilityID": "1000051",
            "id": 45,
            "location": "4887 Bruns Road Byron CA 94514",
            "m19": 24,
            "m18": 65245,
            "m17": 79875,
            "m16": 27,
            "m15": 26,
            "m14": 27,
            "m13": 44463,
            "m12": 43848,
            "m11": 0,
            "m10": 0,
            "county_id": 50,
            "county": "Contra Costa County",
            "parents": ["MITSUBISHI CORP (AMERICAS)"],
            "emission_history": [0, 0, 43848, 44463, 27, 26, 27, 79875, 65245, 24, 28],
            "percent_sector": "0.068481033",
            "mostcommonemission": "Carbon Dioxide",
            "name": "Mariposa Energy, LLC",
            "openlandfill": None,
            "parentc": "MITSUBISHI CORP (AMERICAS) (100%)",
            "sector": "Power Plants",
            "state": "CA",
            "zip": "94514",
        }

        data = db.session.query(Facility).filter((Facility.id) == 45).first()
        assert data
        assert mariposa_energy["id"] == data.id
        assert mariposa_energy["address"] == data.address
        assert mariposa_energy["city"] == data.city
        assert mariposa_energy["county_id"] == data.county_id
        assert mariposa_energy["emissions"] == data.emissions
        assert mariposa_energy["facilityID"] == data.facilityID
        assert mariposa_energy["location"] == data.location
        assert mariposa_energy["m19"] == data.m19
        assert mariposa_energy["m18"] == data.m18
        assert mariposa_energy["m17"] == data.m17
        assert mariposa_energy["m16"] == data.m16
        assert mariposa_energy["m15"] == data.m15
        assert mariposa_energy["m14"] == data.m14
        assert mariposa_energy["m13"] == data.m13
        assert mariposa_energy["m12"] == data.m12
        assert mariposa_energy["m11"] == data.m11
        assert mariposa_energy["m10"] == data.m10
        assert mariposa_energy["county_id"] == data.county_id
        assert mariposa_energy["county"] == data.county
        assert mariposa_energy["parents"] == data.parents
        assert mariposa_energy["emission_history"] == data.emission_history
        assert mariposa_energy["percent_sector"] == str(data.percent_sector)
        assert mariposa_energy["mostcommonemission"] == data.mostcommonemission
        assert mariposa_energy["name"] == data.name
        assert mariposa_energy["openlandfill"] == data.openlandfill
        assert mariposa_energy["parentc"] == data.parentc
        assert mariposa_energy["sector"] == data.sector
        assert mariposa_energy["state"] == data.state
        assert mariposa_energy["zip"] == data.zip
