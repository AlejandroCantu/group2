from init import db
from sqlalchemy import Column, Integer, String


class link_companies_fac(db.Model):
    __tablename__ = "comp_facs"

    id = db.Column(db.Integer, primary_key=True)
    company_id = db.Column(db.Integer, db.ForeignKey("companies.id"))
    facility_id = db.Column(db.Integer, db.ForeignKey("facilities.id"))

    def __init__(self, company_id, facility_id):
        self.company_id = company_id
        self.facility_id = facility_id


class Facility(db.Model):
    __tablename__ = "facilities"

    id = db.Column(db.Integer, primary_key=True)
    facilityID = db.Column(db.String())
    name = db.Column(db.String())
    location = db.Column(db.String())
    zip = db.Column(db.String())
    emissions = db.Column(db.Integer())
    parentc = db.Column(db.Integer())
    sector = db.Column(db.String())
    openlandfill = db.Column(db.String())
    mostcommonemission = db.Column(db.String())
    address = db.Column(db.String())
    city = db.Column(db.String())
    state = db.Column(db.String())
    zip = db.Column(db.String())
    m19 = db.Column(db.Integer)
    m18 = db.Column(db.Integer)
    m17 = db.Column(db.Integer)
    m16 = db.Column(db.Integer)
    m15 = db.Column(db.Integer)
    m14 = db.Column(db.Integer)
    m13 = db.Column(db.Integer)
    m12 = db.Column(db.Integer)
    m11 = db.Column(db.Integer)
    m10 = db.Column(db.Integer)
    county_id = db.Column(db.Integer, db.ForeignKey("counties.id"))
    county = db.Column(db.String())
    parents = db.Column(db.ARRAY(db.String()))
    emission_history = db.Column(db.ARRAY(db.Integer()))
    percent_sector = db.Column(db.Numeric())

    def __init__(
        self,
        facilityID,
        name,
        loc,
        emiss,
        address,
        city,
        state,
        zip,
        parent,
        sector,
        open,
        most,
        m19,
        m18,
        m17,
        m16,
        m15,
        m14,
        m13,
        m12,
        m11,
        m10,
        county_id,
        county,
        parents,
        emission_history,
        percent_sector,
    ):
        self.facilityID = facilityID
        self.name = name
        self.location = loc
        self.zip = zip
        self.emissions = emiss
        self.parentc = parent
        self.sector = sector
        self.openlandfill = open
        self.mostcommonemission = most
        self.address = address
        self.city = city
        self.state = state
        self.zip = zip
        self.m19 = m19
        self.m18 = m18
        self.m17 = m17
        self.m16 = m16
        self.m15 = m15
        self.m14 = m14
        self.m13 = m13
        self.m12 = m12
        self.m11 = m11
        self.m10 = m10
        self.county_id = county_id
        self.county = county
        self.parents = parents
        self.emission_history = emission_history
        self.percent_sector = percent_sector

    def __repr__(self):
        return "<id {}>".format(self.id)

    def as_dict(self):
        return {
            "id": self.id,
            "facilityID": self.facilityID,
            "name": self.name,
            "address": self.address,
            "city": self.city,
            "state": self.state,
            "zip": self.zip,
            "emissions": self.emissions,
            "mostcommonemission": self.mostcommonemission,
            "location": self.location,
            "parentc": self.parentc,
            "sector": self.sector,
            "openlandfill": self.openlandfill,
            "m19": self.m19,
            "m18": self.m18,
            "m17": self.m17,
            "m16": self.m16,
            "m15": self.m15,
            "m14": self.m14,
            "m13": self.m13,
            "m12": self.m12,
            "m11": self.m11,
            "m10": self.m10,
            "county_id": self.county_id,
            "county": self.county,
            "parents": self.parents,
            "emission_history": self.emission_history,
            "percent_sector": self.percent_sector,
        }


class Company(db.Model):
    __tablename__ = "companies"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    address = db.Column(db.String())
    city = db.Column(db.String())
    state = db.Column(db.String())
    zip = db.Column(db.String())
    website = db.Column(db.String())
    subsidiaries = db.Column(db.Integer())
    branches = db.Column(db.Integer())
    founded = db.Column(db.String())
    revenue = db.Column(db.String())
    description = db.Column(db.String())
    linked_in = db.Column(db.String())
    facebook = db.Column(db.String())
    twitter = db.Column(db.String())
    phone = db.Column(db.String())
    fax = db.Column(db.String())
    emissions = db.Column(db.Integer())
    email = db.Column(db.String())
    emissions = db.Column(db.Integer())
    sector = db.Column(db.String())
    county_id = db.Column(db.Integer, db.ForeignKey("counties.id"))

    def __init__(
        self,
        name,
        address,
        city,
        state,
        zip,
        website,
        subsidiaries,
        branches,
        founded,
        revenue,
        description,
        linked_in,
        facebook,
        twitter,
        phone,
        fax,
        email,
        emissions,
        sector,
        county_id,
    ):
        self.name = name
        self.address = address
        self.city = city
        self.state = state
        self.zip = zip
        self.website = website
        self.subsidiaries = subsidiaries
        self.branches = branches
        self.zip = zip
        self.founded = founded
        self.revenue = revenue
        self.description = description
        self.linked_in = linked_in
        self.facebook = facebook
        self.twitter = twitter
        self.phone = phone
        self.fax = fax
        self.email = email
        self.emissions = emissions
        self.sector = sector
        self.county_id = county_id

    def __repr__(self):
        return "<id {}>".format(self.id)

    def as_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "address": self.address,
            "city": self.city,
            "state": self.state,
            "zip": self.zip,
            "website": self.website,
            "subsidiaries": self.subsidiaries,
            "branches": self.branches,
            "founded": self.founded,
            "revenue": self.revenue,
            "description": self.description,
            "linked_in": self.linked_in,
            "facebook": self.facebook,
            "twitter": self.twitter,
            "phone": self.phone,
            "fax": self.fax,
            "email": self.email,
            "sector": self.sector,
            "emissions": self.emissions,
            "county_id": self.county_id,
        }


class Official(db.Model):
    __tablename__ = "officials"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    website = db.Column(db.String)
    party = db.Column(db.String)
    phone = db.Column(db.String)
    email = db.Column(db.String)
    twitter = db.Column(db.String)
    facebook = db.Column(db.String)
    office = db.Column(db.String)
    county_id = db.Column(db.Integer, db.ForeignKey("counties.id"))

    def __init__(
        self, name, website, party, phone, email, twitter, facebook, office, county_id
    ):
        self.name = name
        self.website = website
        self.party = party
        self.phone = phone
        self.email = email
        self.twitter = twitter
        self.facebook = facebook
        self.office = office
        self.county_id = county_id

    def __repr__(self):
        return "<Official %s>" % self.name

    def as_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "website": self.website,
            "party": self.party,
            "email": self.email,
            "twitter": self.twitter,
            "facebook": self.facebook,
            "office": self.office,
            "county_id": self.county_id,
        }


class County(db.Model):
    __tablename__ = "counties"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    # Variables
    state_val = db.Column(db.String)
    info = db.Column(db.String)
    emissions = db.Column(db.Integer)
    population = db.Column(db.Integer)
    img_url = db.Column(db.String)

    # One-to-Many relationships
    official_id = db.relationship("Official", backref="county", lazy=True)
    company_id = db.relationship("Company", backref="company", lazy=True)
    facility_id = db.relationship("Facility", backref="facility", lazy=True)

    demographic = db.Column(db.String)
    ethnicity = db.Column(db.String)
    income = db.Column(db.String)

    def __init__(
        self,
        id,
        name,
        emissions,
        population,
        info,
        state_val,
        demographic,
        ethnicity,
        income,
        img_url,
    ):
        self.id = id
        self.name = name
        self.state_val = state_val
        self.emissions = emissions
        self.population = population
        self.info = info
        self.demographic = demographic
        self.ethnicity = ethnicity
        self.income = income
        self.img_url = img_url

    def __repr__(self):
        return "<County %s>" % self.name

    def as_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "state": self.state_val,
            "demographic": self.demographic,
            "info": self.info,
            "emissions": self.emissions,
            "population": self.population,
            "income": self.income,
            "ethnicity": self.ethnicity,
            "img_url": self.img_url,
        }
