from models import Facility, Company, County, db
from query_helper import *
from sqlalchemy import func, or_


def filter_facility_by(fac_query, filtering, what):
    if filtering == "state":
        fac_query = fac_query.filter(Facility.state == what)

    if filtering == "sector":
        fac_query = fac_query.filter(Facility.sector == what)
    return fac_query


def filter_facilities(queries, fac_query):
    state = get_query("state", queries)
    sector = get_query("sector", queries)

    if state != None:
        fac_query = filter_facility_by(fac_query, "state", state)

    if sector != None:
        fac_query = filter_facility_by(fac_query, "sector", sector)

    return fac_query


def sort_facility_by(sorting, fac_query, desc):
    fac = None

    try:
        if sorting["sort"] == "emissions":
            fac = Facility.emissions

        elif sorting["sort"] == "name":
            fac = Facility.name

        elif sorting["sort"] == "sector":
            fac = Facility.sector

        elif sorting["sort"] == "per_sector_emissions":
            fac = Facility.percent_sector
    except KeyError:
        return fac_query

    if desc:
        return fac_query.order_by(fac.desc())
    else:
        return fac_query.order_by(fac.asc())


def sort_facilities(sort, fac_query):
    if sort == None:
        return fac_query

    try:
        # descending
        if sort["desc"] == "true":
            return sort_facility_by(sort, fac_query, True)
        # ascending
        else:
            return sort_facility_by(sort, fac_query, False)
    except KeyError:
        return sort_facility_by(sort, fac_query, True)


def search_facilities(q, fac_query):
    if not q:
        return fac_query
    else:
        try:
            terms = q["q"].split()
        except KeyError:
            return fac_query

    searches = []
    for term in terms:
        try:
            searches.append(Facility.facilityID.ilike("%{}%".format(term.lower())))
        except ValueError:
            pass

        searches.append(
            func.lower(Facility.location).ilike("%{}%".format(term.lower()))
        )
        searches.append(func.lower(Facility.name).ilike("%{}%".format(term.lower())))
        searches.append(func.lower(Facility.city).ilike("%{}%".format(term.lower())))
        searches.append(func.lower(Facility.state).ilike("%{}%".format(term.lower())))
        searches.append(func.lower(Facility.zip).ilike("%{}%".format(term.lower())))
        searches.append(func.lower(Facility.sector).ilike("%{}%".format(term.lower())))
        searches.append(
            func.lower(Facility.county).ilike(
                "%{}%".format(term.lower())
            )  ## need to get county name from county id
        )
    print("Thing: ", searches[0], searches[1], searches[2])
    ret = fac_query.filter(or_(*tuple(searches)))
    for r in ret:
        print("THis: ", r)
    print("Thing: ", ret)

    return ret
