from sqlalchemy import and_, or_, func
from models import Company, County


def filter_companies(filter, by, query):
    if not filter:
        return query

    if by == "state":
        return query.filter(Company.state == filter)
    elif by == "zip":
        return query.filter(Company.zip == filter)
    elif by == "city":
        return query.filter(Company.city == filter)

    return query


def sort_companies(sort, desc, query):
    if not sort:
        return query
    sortby = None
    if sort == "name":
        sortby = Company.name
    elif sort == "revenue":
        sortby = Company.revenue
    elif sort == "emissions":
        sortby = Company.emissions
    elif sort == "founded":
        sortby = Company.founded
    elif sort == "subsidiaries":
        sortby = Company.subsidiaries
    else:
        return query

    if desc:
        return query.order_by(sortby.desc())
    else:
        return query.order_by(sortby)


# Adapted with help from Texasvotes https://gitlab.com/forbesye/fitsbits/-/blob/master/back-end/Election.py
def search_companies(q, query):
    if not q:
        return query

    terms = q.split()

    searches = []
    for term in terms:
        searches.append(Company.name.ilike("%{}%".format(term)))
        searches.append(Company.city.ilike("%{}%".format(term)))
        searches.append(Company.state.ilike("%{}%".format(term)))
        searches.append(Company.zip.ilike("%{}%".format(term)))
        searches.append(Company.website.match(term))
        try:
            searches.append(Company.subsidiaries.in_([int(term)]))
        except ValueError:
            pass
        searches.append(Company.founded.ilike("%{}%".format(term)))
        searches.append(Company.revenue.ilike("%{}%".format(term)))
    query = query.filter(or_(*tuple(searches)))

    return query
