.DEFAULT_GOAL := run
SHELL         := bash

# For default; just runs the website
run:
	cd ./frontend/
	npm start

# Install all libraries for running the front-end
frontend_install:
	cd ./frontend/
	npm install --legacy-peer-deps

test_selenium:
	cd ./frontend/frontend-tests/ && chmod +x chromedriver_linux
	cd ./frontend/ && python3 guitests.py

test_jest:
	cd ./frontend/ && npm install --legacy-peer-deps && npm test

unittest:
	cd ./backend/ &&  python3 -m pytest unittests.py
