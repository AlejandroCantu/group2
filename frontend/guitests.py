import os
from sys import platform


def run_tests(loc):
    os.system("python3 ./frontend-tests/navbar_gui_test.py " + loc)
    os.system("python3 ./frontend-tests/counties_gui_test.py " + loc)
    os.system("python3 ./frontend-tests/facilities_gui_test.py " + loc)
    os.system("python3 ./frontend-tests/companies_gui_test.py " + loc)


if __name__ == "__main__":
    if platform == "win32":
        loc = "./frontend-tests/chromedriver.exe"
    elif platform == "linux":
        loc = "./frontend-tests/chromedriver_linux"
    run_tests(loc)
