from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import sys
import unittest
import time

PATH = ""


class TestNavbar(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.link = "https://www.findingfootprints.me/"

        ops = Options()
        ops.add_argument("--headless")
        ops.add_argument("--disable-gpu")
        ops.add_argument("--window-size=1280,800")
        ops.add_argument("--allow-insecure-localhost")
        ops.add_argument("--log-level=3")
        ops.add_argument("--no-sandbox")
        ops.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(PATH, options=ops)
        cls.driver.get(cls.link)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testHome(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.LINK_TEXT, "Counties"))
            )
        except Exception as e:
            self.assertEqual(True, False)
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Counties").text, "Counties"
        )
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Companies").text, "Companies"
        )
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Facilities").text, "Facilities"
        )

    def testNavigate(self):
        self.driver.get(self.link)
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.LINK_TEXT, "Counties"))
            )
        except Exception as e:
            self.assertEqual(True, False)
        self.driver.find_element(By.LINK_TEXT, "Counties").click()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Counties")
        self.driver.back()
        time.sleep(2)
        self.driver.find_element(By.LINK_TEXT, "Companies").click()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Companies")
        self.driver.back()
        time.sleep(2)
        self.driver.find_element(By.LINK_TEXT, "Facilities").click()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Facilities")
        self.driver.back()
        time.sleep(2)
        self.driver.find_element(By.LINK_TEXT, "About").click()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "About Us")
        self.driver.back()


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["removed"])
