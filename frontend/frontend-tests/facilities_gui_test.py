from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import sys
import unittest
import time

PATH = ""


class TestFacilities(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.link = "https://www.findingfootprints.me/Facilities"

        ops = Options()
        ops.add_argument("--headless")
        ops.add_argument("--disable-gpu")
        ops.add_argument("--window-size=1280,800")
        ops.add_argument("--allow-insecure-localhost")
        ops.add_argument("--log-level=3")
        ops.add_argument("--no-sandbox")
        ops.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(PATH, options=ops)
        cls.driver.get(cls.link)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def waitForLoad(self):
        try:
            a = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.LINK_TEXT, "Counties"))
            )
        except Exception as e:
            self.assertEqual(True, False)

    def testFacilityList(self):
        self.driver.get(self.link)
        self.waitForLoad()
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Counties").text, "Counties"
        )
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Companies").text, "Companies"
        )
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Facilities").text, "Facilities"
        )

    def testFacilityNavigate(self):
        self.driver.get(self.link)
        self.waitForLoad()
        self.driver.find_element(By.LINK_TEXT, "Counties").click()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Counties")
        self.driver.back()
        time.sleep(2)
        self.driver.find_element(By.LINK_TEXT, "Companies").click()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Companies")
        self.driver.back()
        time.sleep(2)
        self.driver.find_element(By.LINK_TEXT, "FindingFootprints").click()
        time.sleep(2)
        self.driver.back()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "Facilities")
        time.sleep(2)
        self.driver.find_element(By.LINK_TEXT, "About").click()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, "About Us")
        self.driver.back()

    def testFacilityInstance(self):
        self.driver.get(self.link)
        self.waitForLoad()
        time.sleep(5)
        instance = self.driver.find_element(By.TAG_NAME, "td")
        button = instance.find_element(By.XPATH, "..")
        name = instance.text
        print(instance.text)
        try:
            button.click()
        except Exception:
            time.sleep(2)
            button.click()
        time.sleep(2)
        self.assertEqual(self.driver.find_element(By.TAG_NAME, "h1").text, name)
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Counties").text, "Counties"
        )
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Companies").text, "Companies"
        )
        self.assertEqual(
            self.driver.find_element(By.LINK_TEXT, "Facilities").text, "Facilities"
        )
        self.driver.back()


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["removed"])
