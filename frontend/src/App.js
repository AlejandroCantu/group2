import "bootstrap/dist/css/bootstrap.min.css";
import About from "./components/About/About.js";
import Home from "./components/Splash/Home.js";
import Companies from "./components/Companies.js";
import Visualizations from "./components/Visualizations.js";
import Provider from "./components/Provider.js";
import Counties from "./components/Counties.js";
import Facilities from "./components/Facilities.js";
import Search from "./components/Search/Search.js";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import MainNavbar from "./components/MainNavbar.js";
import CompanyInstance from "./components/Companies/CompanyInstance.js";
import FacilityInstance from "./components/Facilities/FacilityInstance.js";
import CountyInstance from "./components/Counties/CountyInstance.js";
import "./index.css";

function App() {
  return (
    <div className="App-background">
      <MainNavbar></MainNavbar>
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/search" element={<Search />} />
          <Route path="/facilities" element={<Facilities />} />
          <Route
            path="/facilities/:facilityId"
            element={<FacilityInstance />}
          />
          <Route path="/companies" element={<Companies />} />
          <Route path="/companies/:companyId" element={<CompanyInstance />} />
          <Route path="/counties" element={<Counties />} />
          <Route path="/counties/:countyId" element={<CountyInstance />} />
          <Route path="/visualizations" element={<Visualizations />} />
          <Route path="/provider" element={<Provider />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
