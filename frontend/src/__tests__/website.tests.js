import { configure, shallow } from "enzyme"
import Adapter from "@wojtekmaj/enzyme-adapter-react-17"
import MainNavbar from "../components/MainNavbar";
import About from "../components/About/About"
import CountyInstance from "../components/Counties/CountyInstance";
import CountyList from "../components/Counties/CountyList"
import CompanyList from "../components/Companies/CompanyList"
import CompanyInstance from "../components/Companies/CompanyInstance";
import FacilityInfo from "../components/Facilities/FacilityInfo";
import SearchCard from "../components/Search/SearchCard";
import App from "../App"
import FacilitySort from "../components/Facilities/FacilitySort";

// Help from Texas Votes https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/__tests__/main.tests.js for test formats / use with enzyme

configure({ adapter: new Adapter() })

describe("Base Tests", () => {
    test("Navbar", () => {
        const navbar = shallow(<MainNavbar />)
        expect(navbar).toMatchSnapshot()
    })
    test("About", () => {
        const about = shallow(<About />)
        expect(about).toMatchSnapshot()
    })
    test("Home", () => {
        const home = shallow(<SearchCard />)
        expect(home).toMatchSnapshot()
    })
    test("Index", () => {
        const index = shallow(<App />)
        expect(index).toMatchSnapshot()
    })
})

describe("Counties Tests", () => {
    test("CountyInstance", () => {
        const county = shallow(<CountyInstance data={{
            name: "name",
            state: "state",
            img: "https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg",
            desc: "desc",
            website: "www.google.com",
            social_media: [
                {
                    url: "www.youtube.com",
                    social: "Youtube",
                }
            ],
            area: "420",
            representative: "Lord of all Code, Kyle.",
            repNumber: ">9000",
            contact: "911",
            demographic: "https://i.kym-cdn.com/photos/images/original/001/211/814/a1c.jpg",
            facilities: [
                {
                    facilitiyKey: 0,
                    facility: "My House",
                },
            ],
            companies: [
                {
                    key: 0,
                    company: "Barry's Vet Shop LLC"
                }
            ]
        }}/>)
        expect(county).toMatchSnapshot()
    })
    test("County Page", () => {
        const county_page = shallow(<CountyList oldList={[]} />)
        expect(county_page).toMatchSnapshot()
    })
})

describe("Companies Tests", () => {
    test("CompanyInstance", () => {
        const company = shallow(<CompanyInstance />)
        expect(company).toMatchSnapshot()
    })
    test("Company Page", () => {
        const company_page = shallow(<CompanyList companies={[]} />)
        expect(company_page).toMatchSnapshot()
    })
})

describe("Facilities Tests", () => {
    test("FacilityInstance", () => {
        const facility = shallow(<FacilityInfo data={{
            name: "Name",
            location: "here",
            emissions: "5",
            parentC: "none",
            sector: "somewhere",
            id: 0,
        }}/>)
        expect(facility).toMatchSnapshot()
    })
    test("Facility Sort Page", () => {
        const facility_page = shallow(<FacilitySort />)
        expect(facility_page).toMatchSnapshot()
    })
})