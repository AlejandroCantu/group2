import { React, useEffect, useState } from "react";
import FacilitySort from"./Facilities/FacilitySort.js";
import FacilityFilter from"./Facilities/FacilityFilter.js";
import { Container, Row } from "react-bootstrap";
import { CircularProgress, TextField } from "@mui/material";
import { Link, useNavigate, useSearchParams } from 'react-router-dom';
import { Paper, Table, TableBody, TableCell, TableHead, TablePagination, TableRow, Button, Stack } from '@mui/material';
import Highlight from "./Highlight/Highlight.js";

function Facilities() {
  let [searchParams, setSearchParams] = useSearchParams();
  let page = parseInt(searchParams.get("page") ?? "0")
  let per_page = parseInt(searchParams.get("per_page") ?? "10")
  let query = searchParams.get("q")
  const [searchQ, setSearch] = useState("")
  let sort = searchParams.get("sort")
  let state = searchParams.get("state")
  let sector = searchParams.get("sector")
  let desc = searchParams.get("desc")
  let [facilities, setFacilities] = useState([])
  let [numInstances, setInstances] = useState(0)
  let [, setPageCount] = useState(0)
  let [, setStart] = useState(0)
  let [, setEnd] = useState(0)
  let [loaded, setLoaded] = useState(Boolean)
  let navigate = useNavigate();


  const clearAll = () => {
    let path = ``
    navigate(path)
  }

  const updateSearch = (search) => {
    let path = `?page=0&per_page=${per_page}&q=${search}`
    if (sort){
      path += `&sort=${sort}`
    }
    if (state) {
      path += `&state=${state}`
    }
    if (sector) {
      path += `&sector=${sector}`
    }
    navigate(path)
  }

  const updateSort = (sort, desc) => {
    let path = `?page=0&per_page=${per_page}&sort=${sort}&desc=${desc}`
    if(query){
      path += `&q=${query}`
    }
    if (state) {
      path += `&state=${state}`
    }
    if (sector) {
      path += `&sector=${sector}`
    }
    navigate(path)
  }

  const updateFilter = (filter, what) => {
    if (filter === "state") {
      state = what
    }
    if (filter === "sector") {
      sector = what
    }
    let path = `?page=0&per_page=${per_page}&`
    if (state) {
      path += `&state=${state}`
    }
    if (sector) {
      path += `&sector=${sector}`
    }
    if(query){
      path += `&q=${query}`
    }
    if (sort){
      path += `&sort=${sort}`
    }
    navigate(path)
  }

  const handlePageChange = (event, params) => {
    searchParams.set('page', (params).toString());
    setSearchParams(searchParams);
  };

  const handleRowsPerPageChange = (event) => {
    searchParams.set('per_page', event.target.value.toString());
    setSearchParams(searchParams);
  }

  useEffect(() => {
    const getData = async () => {
      setLoaded(false)
      let url = `https://api.findingfootprints.me/facilities?page=${page}&per_page=${per_page}`
      if (query) {
        url += `&q=${query}`
      }
      if (sort) {
        url += `&sort=${sort}`
      }
      if (desc) {
        url += `&desc=${desc}`
      }
      if (state) {
        url += `&state=${state}`
      }
      if (sector) {
        url += `&sector=${sector}`
      }

      let response = await fetch (url);
      let body = []
      body = await response.json()

      setInstances(body['numInstances'])
      setFacilities(body['list'])
      setPageCount(body['pageCnt'])
      setStart(body['startIdx'])
      setEnd(body['endIdx'])
      setLoaded(true)
    };
    getData();

  }, [page, per_page, query, sort, desc, state, sector]);

  if(!loaded){
    return <CircularProgress />
  }
  return (
    <Container fixed>
      <h1 className="wrapper">Facilities</h1>
      <Row>
        <h2 className="wrapper">Search</h2>
        <TextField onKeyPress={(ev) => {
          if (ev.key === "Enter") {
            ev.preventDefault();
            updateSearch(ev.target.value)
          }
        }}
          label="Search"
          value={searchQ}
          onChange={event => setSearch(event.target.value)}
        >
        </TextField>
      </Row>
      <Row>
        <h2 className="wrapper">Filters and Sorts</h2>
        <Stack direction="row" alignItems="center" >
              <FacilityFilter update={updateFilter} state={state} sector={sector} />
              <FacilitySort update={updateSort} sort={sort} desc ={desc} />
              <Button variant="outlined" style={{width: "200px", height: "50px"}} onClick = {() => clearAll()}>Clear</Button>
        </Stack>
      </Row>
      <Row>
          <Paper outlined style={{marginBottom: "32px"}}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Address</TableCell>
                    <TableCell>City</TableCell>
                    <TableCell align="center">State</TableCell>
                    <TableCell align="right">Zip Code</TableCell>
                    <TableCell align="right">Emissions (in tons)</TableCell>
                    <TableCell align="right">Most Common Emission</TableCell>
                    <TableCell align="right">Sector</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {facilities.map((facility) => (
                    <TableRow className="tableRow"
                      hover
                      key={facility.id}
                      component={Link}
                      to={`id=${facility.id}`}
                      sx={{
                        textDecoration: 'none',
                      }}
                    >
                    <TableCell sx={{width: "300px"}}><Highlight by={query}>{facility.name}</Highlight></TableCell>
                    <TableCell sx={{width: "200px"}}><Highlight by={query}>{facility.address}</Highlight></TableCell>
                    <TableCell><Highlight by={query}>{facility.city}</Highlight></TableCell>
                    <TableCell align="center"><Highlight by={query}>{facility.state}</Highlight></TableCell>
                    <TableCell align="right"><Highlight by={query}>{facility.zip}</Highlight></TableCell>
                    <TableCell align="right"><Highlight by={query}>{facility.emissions}</Highlight></TableCell>
                    <TableCell align="right"><Highlight by={query}>{facility.mostcommonemission}</Highlight></TableCell>
                    <TableCell align="right"><Highlight by={query}>{facility.sector}</Highlight></TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
              <TablePagination
                showFirstButton
                showLastButton
                rowsPerPageOptions={[10, 15, 20]}
                count={numInstances}
                rowsPerPage={per_page}
                page={page}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleRowsPerPageChange}
              />
          </Paper>
      </Row>
    </Container>
  );

}
export default Facilities;
