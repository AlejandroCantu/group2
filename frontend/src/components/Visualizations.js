import { React } from "react";
import { Container } from "react-bootstrap";
import CompanyEmissionsChart from "./Visualizations/CompanyEmissionsChart";
import FacilityEmissionsChart from "./Visualizations/FacilityEmissionsChart";
import CountyEmissionsChart from "./Visualizations/CountyEmissionsChart";
import { Stack } from "@mui/material"
import "../styles/Visualizations.css"

function Visualizations() {

  return (
    <Container>
      <h1 className="wrapper">Our Visualizations</h1>

      <Stack>
      <h1 className="wrapperTitle">Top 30 Companies Emissions by Sector</h1>
      <CompanyEmissionsChart />

      <h1 className="wrapperTitle">Top 20 Emissions Over Time by Facility</h1>
      <FacilityEmissionsChart />

      
      <h1 className="wrapperTitle">Top 35 County Emissions by State</h1>
      <CountyEmissionsChart />
      </Stack>

    </Container>
  );

}
export default Visualizations;
