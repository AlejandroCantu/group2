import { React, useEffect, useState } from "react";
import CompanyList from "./Companies/CompanyList.js";
import CompanyFilter from "./Companies/CompanyFilter.js";
import { Container, Row } from "react-bootstrap";
import { CircularProgress, TextField } from "@mui/material";
import CompanySort from "./Companies/CompanySort.js";
import {
  Stack,
  Pagination,
  PaginationItem,
  Button
} from "@mui/material";
import { Link, useNavigate, useSearchParams } from 'react-router-dom';
import PerPagePicker from "./PerPagePicker.js";

function getPath({ page = 1, per_page = 12, query, sort, desc, stateFilter, zipFilter, cityFilter } = {}) {
  let path = `?page=${page}&per_page=${per_page}`
  if(query){
    path += `&q=${query}`
  }
  if(sort){
    path += `&sort=${sort}`
  }
  if(desc){
    path += `&descending=true`
  }
  if(stateFilter) {
    path += `&state=${stateFilter}`
  }
  if(zipFilter) {
    path += `&zip=${zipFilter}`
  }
  if(cityFilter) {
    path += `&city=${cityFilter}`
  }
  return path
}

function Companies() {
  let [searchParams] = useSearchParams();
  
  let page = parseInt(searchParams.get("page") ?? "1")
  let per_page = parseInt(searchParams.get("per_page") ?? "12")
  let query = searchParams.get("q")
  const [searchQ, setSearch] = useState("")
  let sort = searchParams.get("sort") ?? "name"
  let stateFilter = searchParams.get("state")
  let zipFilter = searchParams.get("zip")
  let cityFilter = searchParams.get("city")
  let desc = searchParams.get("descending")
  let [companies, setCompanies] = useState([])
  let [numInstances, setInstances] = useState(0)
  let [pageCount, setPageCount] = useState(0)
  let [startIndex, setStart] = useState(0)
  let [endIndex, setEnd] = useState(0)
  let [loaded, setLoaded] = useState(Boolean)
  let navigate = useNavigate()

  const clearAll = () => {
    let path = ``
    navigate(path)
  } 

  const updateSearch = (search) => {
    let path = getPath({ page: 1, per_page: per_page, query: search, sort: sort, desc: desc, stateFilter: stateFilter, zipFilter: zipFilter, cityFilter: cityFilter })
    setSearch(search)
    navigate(path)
  }

  const updatePerPage = (num) => {
    let path = getPath({ page: 1, per_page: num, query: query, sort: sort, desc: desc, stateFilter: stateFilter, zipFilter: zipFilter, cityFilter: cityFilter })
    navigate(path)
  }

  const updateSort = (sort) => {
    let path = getPath({ page: page, per_page: per_page, query: query, sort: sort, desc: desc, stateFilter: stateFilter, zipFilter: zipFilter, cityFilter: cityFilter })
    navigate(path)
  }

  const updateOrder = (desc) => {
    let path = getPath({ page: page, per_page: per_page, query: query, sort: sort, desc: desc, stateFilter: stateFilter, zipFilter: zipFilter, cityFilter: cityFilter })
    navigate(path)
  } 

  const updateFilter = (by, filter) => {
    if(by === "state") {
      stateFilter = filter
    }else if(by === "zip") {
      zipFilter = filter
    }else if(by === "city") {
      cityFilter = filter
    }
    let path = getPath({ page: 1, per_page: per_page, query: query, sort: sort, desc: desc, stateFilter: stateFilter, zipFilter: zipFilter, cityFilter: cityFilter })
    navigate(path)
  }

  useEffect(() => {
    setLoaded(false)
    const getData = async () => {
      let url = `https://api.findingfootprints.me/companies?page=${page}&per_page=${per_page}`
      if(query){
        url += `&q=${query}`
      }
      if(sort){
        url += `&sort=${sort}`
      }
      if(desc) {
        url += `&descending=true`
      }
      if(stateFilter) {
        url += `&state=${stateFilter}`
      }
      if(zipFilter) {
        url += `&zip=${zipFilter}`
      }
      if(cityFilter) {
        url += `&city=${cityFilter}`
      }
      let response = await fetch (
        url
      );
      let body = []
      body = await response.json()
      setInstances(body['numInstances'])
      setCompanies(body['list'])
      setPageCount(body['pageCnt'])
      setStart(body['startIdx'])
      setEnd(body['endIdx'])
      setLoaded(true)
    };
    getData();
  }, [page, per_page, query, sort, desc, stateFilter, zipFilter, cityFilter]);


  
  if(!loaded){
    return <CircularProgress />
  }
  return (
    <Container>
      <h1 className="wrapper">Companies</h1>
       <Row>
          <h2 className="wrapper">Search</h2>
          <TextField onKeyPress={(ev) => {
            if(ev.key === "Enter"){
              ev.preventDefault();
              updateSearch(ev.target.value)
            }
          }}
          label="Search"
          value={searchQ}
          onChange={event => setSearch(event.target.value)}
          />
      </Row>
    
      <h2 className="wrapper">Filters and Sorts</h2>
      <Stack direction="row" >
      <CompanyFilter update={updateFilter} state={stateFilter} city={cityFilter} zip={zipFilter}/>
      <CompanySort update={updateSort} updateOrder={updateOrder} sort={sort} desc={desc}/>
      </Stack>
      <Stack alignItems="center" direction="row" gap="32px">
        <Container>
        <PerPagePicker update={updatePerPage} num={per_page}/>
        </Container>
        <Container>
        <Button variant="outlined" style={{width: "200px", height: "50px"}} onClick = {() => clearAll()}>Clear</Button>
        </Container>
      </Stack>
    
      <Row>
        <CompanyList companies={companies} search={query} />
        <Row>
         <h3 className="text-center mt-5">
           Viewing Companies {startIndex} - {Math.min(numInstances, endIndex)} of {numInstances}
         </h3>
       </Row>
       <div style={{display: 'flex', justifyContent: 'center', marginBottom: "32px"}}>
            <Pagination showFirstButton="true" showLastButton="true" shape="rounded" count={pageCount} defaultPage={page} renderItem={(item) => (
              <PaginationItem
                component={Link}
                to={
                  `?page=${item.page}&per_page=${per_page}`
                    + (query ? `&q=${query}` : ``)
                    + (sort ? `&sort=${sort}`: ``)
                    + (desc ? `&descending=true`: ``)
                    + (stateFilter ? `&state=${stateFilter}` : ``)
                }
                {...item}
              />
              )}
            />
        </div>
      </Row>
    </Container>
  );
  
}
export default Companies;
