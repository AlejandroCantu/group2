import { Container } from "react-bootstrap";
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';

function FacilitySort(props) {
    const goodName = {
      "emissions true" : "Emissions High to Low",
      "emissions false" : "Emissions Low to High",
      "name false" : "Name A-Z",
      "sector false" : "Sector A-Z",
    }

    let sort = props.sort + " " + props.desc
    let properName = goodName[sort] 
    if (props.sort === null) {
      sort = "Sort"
    } else {
      sort = properName
    }
    return (
        <Container>
        <FormControl sx={{ m: 1, width: 250 }}>
        <InputLabel id="demo-multiple-name-label">{sort}</InputLabel>
            <Select
            labelId="select-label"
            input={<OutlinedInput label="Income" />}
            >
            <MenuItem onClick={() => props.update("emissions","true")} value="Emissions High to Low">Emissions High to Low</MenuItem>
            <MenuItem onClick={() => props.update("emissions", "false")} value="Emissions Low to High">Emissions Low to High</MenuItem>
            <MenuItem onClick={() => props.update("name", "false")} value="Name A-Z">Name A-Z</MenuItem>
            <MenuItem onClick={() => props.update("sector", "false")} value="Sector A-Z">Sector A-Z</MenuItem>
          </Select>
        </FormControl>
      </Container>
    )
}

export default FacilitySort;