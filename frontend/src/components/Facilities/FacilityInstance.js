import { Container, Row, Col } from "react-bootstrap";
import "../../styles/Counties.css";
import "../../styles/CountyInstance.css";
import "../../styles/Facilities.css";
import { useParams } from 'react-router-dom';
import React, { useEffect, useState } from "react";
import {
  Line,
  LineChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
} from "recharts";
import { CircularProgress } from "@mui/material";

function FacilityInstance(props) {
  let id = useParams().facilityId ?? "1"
  let [facilities, setFacilities] = useState([])
  let [loaded, setLoaded] = useState(Boolean)
  useEffect(() => {
    const getData = async () => {
      setLoaded(false)
      let response = await fetch (
        `https://api.findingfootprints.me/facilities/${id}`
      );
      let body = []
      body = await response.json()
      setFacilities(body)
      setLoaded(true)
    };
    getData();
  }, [id]);


  const data = [
    { x: '2010', emissions: facilities['m10'] },
    { x: '2011', emissions: facilities['m11'] },
    { x: '2012', emissions: facilities['m12'] },
    { x: '2013', emissions: facilities['m13'] },
    { x: '2014', emissions: facilities['m14'] },
    { x: '2015', emissions: facilities['m15'] },
    { x: '2016', emissions: facilities['m16'] },
    { x: '2017', emissions: facilities['m17'] },
    { x: '2018', emissions: facilities['m18'] },
    { x: '2019', emissions: facilities['m19'] },
    { x: '2020', emissions: facilities['emissions'] }
  ];

  if(!loaded)
    return <CircularProgress />
  return (
        <Container>
          <Row className="Card">
            <Col>
              <h1 class="cardTitle">{facilities['name']}</h1>
              <h2 class="cardSub">Location: {facilities['location']}</h2>
              <hr />
            </Col>
            <Row>
              <Col>
                <div class="bodyText">
                  <p>
                    <strong>County: </strong>
                    {facilities['county'] !== undefined &&
                      <a className="county-button" href={"/counties/id=" + facilities['county_id']}> {(facilities['county']['name'])}</a>                  
                    }
                  </p>
                  <p>
                    {" "}
                    <strong>GHG emissions in 2020: </strong> {facilities['emissions']} {" "}
                  </p>
                  <p>
                    {" "}
                    <strong>Owned and operated by: </strong>  {facilities['parentc']} {" "}
                  </p>
                  <p>
                    <strong>Owners: </strong>  {facilities.companies_list.map((comp) => (
                              <a className="fac-button" href={"/companies/id=" + comp.id}> {comp.name} </a>
                        ))}
                  </p>
                  <p>
                    {" "}
                    <strong>Sector: </strong> {facilities['sector']} {" "}
                  </p>
                  <p>
                    {" "}
                    <strong>Most common emission: </strong> {facilities['mostcommonemission']}{" "}
                  </p>
                </div>
              </Col>
              <Col>
                {facilities['location'] !== undefined  &&             
                <iframe
                  width="450"
                  height="250"
                  title="location"
                  frameBorder="10" style={{border: 0}}
                  referrerPolicy="no-referrer-when-downgrade"
                  src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyBTjcfpnazG6FrLPvMvhvvuuHM_PFEhAbM&q=" + encodeURIComponent(facilities['location'].replace(/ /g, "+"))}
                  allowFullScreen>
                </iframe>}
              </Col>
            </Row>
            <Row>
            <LineChart
              width={1000}
              height={600}
              data={data}
              margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="x" />
              <YAxis />
              <Tooltip />
              <Line type="monotone" dataKey="emissions" stroke="#004f62"/>
            </LineChart>
            </Row> 
          </Row>
        </Container>
  );
}

export function spaceString(str) {
  let list = str.match(/[A-Z][a-z]+/g);
  let result = "";
  for (let i = 0; i < list.length; i++) {
    result += list[i] + " ";
  }
  return result;
}

export default FacilityInstance;