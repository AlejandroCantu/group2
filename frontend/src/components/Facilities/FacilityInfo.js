import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
} from "mdb-react-ui-kit";

import { CardActionArea } from "@mui/material";
import { Link as RouterLink } from "react-router-dom";
import "../../styles/Facilities.css";
import { Col, Row, ListGroup, ListGroupItem } from "react-bootstrap";
import React from "react";

function FacilityInfo(props) {
  return (
    <div className="container-group">
      <Row>
        <Col>
          <CardActionArea
            component={RouterLink}
            to={"/facilities/id=" + props.data.id}
          >
            <MDBCard className="facility-card-style">
              <MDBCardBody>
                <MDBCardTitle className="facilityCardTitle">
                  {" "}
                  {props.data.name}{" "}
                </MDBCardTitle>
                <h5 className="location-style">{props.data.location}</h5>
                <MDBCardText>
                  <ListGroup>
                    <ListGroupItem>
                      <strong>Emissions: </strong>
                      {props.data.emissions} metric tons
                    </ListGroupItem>
                    <ListGroupItem>
                      <strong>Parent Company: </strong>
                        {props.data.parentc}
                    </ListGroupItem>
                    <ListGroupItem>
                      <strong>Sector: </strong>
                      {props.data.sector}
                    </ListGroupItem>
                  </ListGroup>
                </MDBCardText>
              </MDBCardBody>
            </MDBCard>
          </CardActionArea>
        </Col>
      </Row>
    </div>
  );
}

export default FacilityInfo;
