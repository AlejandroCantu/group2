import { Container } from "react-bootstrap";
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';


function FacilityFilter(props) {
    let st8 = props.state
    let sec2r = props.sector
    if (props.state === null) {
      st8 = "State"
    }
    if (props.sector === null) {
      sec2r = "Sector"
    }
    return(
      
        <Container>
          <FormControl sx={{ m: 1, width: 200 }}>
          <InputLabel id="demo-multiple-name-label">{st8}</InputLabel>
            <Select
            labelId="select-label"
            input={<OutlinedInput label="States" />}
            >
            
          <MenuItem onClick={() => props.update("state", "AL")} value="AL">AL</MenuItem>
          <MenuItem onClick={() => props.update("state", "AK")} value="AK">AK</MenuItem>
          <MenuItem onClick={() => props.update("state", "AZ")} value="AZ">AZ</MenuItem>
          <MenuItem onClick={() => props.update("state", "AR")} value="AR">AR</MenuItem>
          <MenuItem onClick={() => props.update("state", "CA")} value="CA">CA</MenuItem>
          <MenuItem onClick={() => props.update("state", "CO")} value="CO">CO</MenuItem>
          <MenuItem onClick={() => props.update("state", "CT")} value="CT">CT</MenuItem>
          <MenuItem onClick={() => props.update("state", "DE")} value="DE">DE</MenuItem>
          <MenuItem onClick={() => props.update("state", "FL")} value="FL">FL</MenuItem>
          <MenuItem onClick={() => props.update("state", "GA")} value="GA">GA</MenuItem>
          <MenuItem onClick={() => props.update("state", "HI")} value="HI">HI</MenuItem>
          <MenuItem onClick={() => props.update("state", "ID")} value="ID">ID</MenuItem>
          <MenuItem onClick={() => props.update("state", "IL")} value="IL">IL</MenuItem>
          <MenuItem onClick={() => props.update("state", "IN")} value="IN">IN</MenuItem>
          <MenuItem onClick={() => props.update("state", "IA")} value="IA">IA</MenuItem>
          <MenuItem onClick={() => props.update("state", "KS")} value="KS">KS</MenuItem>
          <MenuItem onClick={() => props.update("state", "KY")} value="KY">KY</MenuItem>
          <MenuItem onClick={() => props.update("state", "LA")} value="LA">LA</MenuItem>
          <MenuItem onClick={() => props.update("state", "ME")} value="ME">ME</MenuItem>
          <MenuItem onClick={() => props.update("state", "MD")} value="MD">MD</MenuItem>
          <MenuItem onClick={() => props.update("state", "MA")} value="MA">MA</MenuItem>
          <MenuItem onClick={() => props.update("state", "MI")} value="MI">MI</MenuItem>
          <MenuItem onClick={() => props.update("state", "MN")} value="MN">MN</MenuItem>
          <MenuItem onClick={() => props.update("state", "MS")} value="MS">MS</MenuItem>
          <MenuItem onClick={() => props.update("state", "MO")} value="MO">MO</MenuItem>
          <MenuItem onClick={() => props.update("state", "MT")} value="MT">MT</MenuItem>
          <MenuItem onClick={() => props.update("state", "NE")} value="NE">NE</MenuItem>
          <MenuItem onClick={() => props.update("state", "NV")} value="NV">NV</MenuItem>
          <MenuItem onClick={() => props.update("state", "NH")} value="NH">NH</MenuItem>
          <MenuItem onClick={() => props.update("state", "NJ")} value="NJ">NJ</MenuItem>
          <MenuItem onClick={() => props.update("state", "NM")} value="NM">NM</MenuItem>
          <MenuItem onClick={() => props.update("state", "NY")} value="NY">NY</MenuItem>
          <MenuItem onClick={() => props.update("state", "NC")} value="NC">NC</MenuItem>
          <MenuItem onClick={() => props.update("state", "ND")} value="ND"> ND</MenuItem>
          <MenuItem onClick={() => props.update("state", "OH")} value="OH">OH</MenuItem>
          <MenuItem onClick={() => props.update("state", "OK")} value="OK">OK</MenuItem>
          <MenuItem onClick={() => props.update("state", "OR")} value="OR">OR</MenuItem>
          <MenuItem onClick={() => props.update("state", "PA")} value="PA">PA</MenuItem>
          <MenuItem onClick={() => props.update("state", "RI")} value="RI">RI</MenuItem>
          <MenuItem onClick={() => props.update("state", "SC")} value="SC">SC</MenuItem>
          <MenuItem onClick={() => props.update("state", "SD")} value="SD">SD</MenuItem>
          <MenuItem onClick={() => props.update("state", "TN")} value="TN">TN</MenuItem>
          <MenuItem onClick={() => props.update("state", "TX")} value="TX">TX</MenuItem>
          <MenuItem onClick={() => props.update("state", "UT")} value="UT">UT</MenuItem>
          <MenuItem onClick={() => props.update("state", "VT")} value="VT">VT</MenuItem>
          <MenuItem onClick={() => props.update("state", "VA")} value="VA">VA</MenuItem>
          <MenuItem onClick={() => props.update("state", "WA")} value="WA">WA</MenuItem>
          <MenuItem onClick={() => props.update("state", "WV")} value="WV">WV</MenuItem>
          <MenuItem onClick={() => props.update("state", "WI")} value="WI">WI</MenuItem>
          <MenuItem onClick={() => props.update("state", "WY")} value="WY">WY</MenuItem>
          </Select>
        </FormControl>

        <FormControl sx={{ m: 1, width: 250 }}>
        <InputLabel id="demo-multiple-name-label">{sec2r}</InputLabel>
            <Select
            labelId="select-label"
            input={<OutlinedInput label="Income" />}
            >
          <MenuItem onClick={() => props.update("sector", "Pulp%20and%20Paper")} value="Pulp and Paper">Pulp and Paper</MenuItem>
          <MenuItem onClick={() => props.update("sector", "Minerals")} value="Minerals">Minerals</MenuItem>
          <MenuItem onClick={() => props.update("sector", "Power%20Plants")} value="Power Plants">Power Plants</MenuItem>
          <MenuItem onClick={() => props.update("sector", "Metals")} value="Metals">Metals</MenuItem>
          <MenuItem onClick={() => props.update("sector", "Petroleum%20and%20Natural%20Gas%20Systems")} value="Petroleum and Natural Gas Systems">Petroleum and Natural Gas Systems</MenuItem>
          <MenuItem onClick={() => props.update("sector", "Chemicals")} value="Chemicals">Chemicals</MenuItem>
          <MenuItem onClick={() => props.update("sector", "Waste")} value="Waste">Waste</MenuItem>
          <MenuItem onClick={() => props.update("sector", "Refineries")} value="Refineries">Refineries</MenuItem>
          <MenuItem onClick={() => props.update("sector", "Other")} value="Other">Other</MenuItem>
          </Select>
        </FormControl>
    </Container>
    )
}
export default FacilityFilter;