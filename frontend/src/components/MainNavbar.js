import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Container from "react-bootstrap/Container";
import NavDropdown from "react-bootstrap/NavDropdown";
import React from "react";

function MainNavbar() {
  return (
    <Navbar
      collapseOnSelect
      className="Navbar-custom"
      expand="lg"
      variant="light"
    >
      <Container>
        <Navbar.Brand className="Title-text" href="/">
          FindingFootprints
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/counties">Counties</Nav.Link>
            <Nav.Link href="/companies">Companies</Nav.Link>
            <Nav.Link href="/facilities">Facilities</Nav.Link>
            <NavDropdown title="Visualizations" id="vis-dropdown">
              <NavDropdown.Item href="/visualizations">Our Visualizations</NavDropdown.Item>
              <NavDropdown.Item href="/provider">Provider Visualizations</NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Nav>
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link eventKey={2} href="/search">
              Search
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default MainNavbar;
