import { Stack } from "@mui/material";
import { Container } from "react-bootstrap";
import { React } from "react";
import "../../styles/Card.css";
import "../../styles/Companies.css";
import CompanyTile from "./CompanyTile";

function CompanyList(props) {
  return (
    <Container>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Stack
          direction="row"
          justifyContent="center"
          flexWrap="wrap"
          gap="32px"
        >
          {props.companies.map((c) => (
            <CompanyTile c={c} search={props.search} />
          ))}
        </Stack>
      </div>
    </Container>
  );
}

export default CompanyList;
