import { Container } from "react-bootstrap";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";

function CompanySort(props) {
  const goodName = {
    name: "Sort By Name",
    revenue: "Revenue",
    emissions: "Facility Emissions",
    founded: "Date Founded",
    subsidiaries: "Number of Subsidiaries",
    true: "Descending",
    false: "Ascending",
  };
  let sort = "";
  if (props.sort === null) {
    sort = "Sort";
  } else {
    sort = goodName[props.sort];
  }
  let order = "";
  if (props.desc === null) {
    order = "Order";
  } else {
    order = goodName[props.desc];
  }
  return (
    <Container>
      <FormControl sx={{ m: 1, width: 200 }}>
        <InputLabel id="demo-multiple-name-label">{sort}</InputLabel>
        <Select labelId="select-label" input={<OutlinedInput label="Sort" />}>
          <MenuItem onClick={() => props.update("name")} value="Company Name">
            Company Name
          </MenuItem>
          <MenuItem onClick={() => props.update("revenue")} value="Revenue">
            Revenue
          </MenuItem>
          <MenuItem
            onClick={() => props.update("emissions")}
            value="Facility Emissions"
          >
            Facility Emissions
          </MenuItem>
          <MenuItem
            onClick={() => props.update("founded")}
            value="Date Founded"
          >
            Date Founded
          </MenuItem>
          <MenuItem
            onClick={() => props.update("subsidiaries")}
            value="Number of Subsidiaries"
          >
            Number of Subsidiaries
          </MenuItem>
        </Select>
      </FormControl>
      <FormControl sx={{ m: 1, width: 200 }}>
        <InputLabel id="demo-multiple-name-label">{order}</InputLabel>
        <Select labelId="select-label" input={<OutlinedInput label="Order" />}>
          <MenuItem onClick={() => props.updateOrder(false)} value="Ascending">
            Ascending
          </MenuItem>
          <MenuItem onClick={() => props.updateOrder(true)} value="Descending">
            Descending
          </MenuItem>
        </Select>
      </FormControl>
    </Container>
  );
}

export default CompanySort;
