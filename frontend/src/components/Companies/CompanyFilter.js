import { Container } from "react-bootstrap";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";

let states = {
  ALABAMA: "AL",
  ALASKA: "AK",
  "AMERICAN SAMOA": "AS",
  ARIZONA: "AZ",
  ARKANSAS: "AR",
  CALIFORNIA: "CA",
  COLORADO: "CO",
  CONNECTICUT: "CT",
  DELAWARE: "DE",
  "DISTRICT OF COLUMBIA": "DC",
  FLORIDA: "FL",
  GEORGIA: "GA",
  GUAM: "GU",
  HAWAII: "HI",
  IDAHO: "ID",
  ILLINOIS: "IL",
  INDIANA: "IN",
  IOWA: "IA",
  KANSAS: "KS",
  KENTUCKY: "KY",
  LOUISIANA: "LA",
  MAINE: "ME",
  MARYLAND: "MD",
  MASSACHUSETTS: "MA",
  MICHIGAN: "MI",
  MINNESOTA: "MN",
  MISSISSIPPI: "MS",
  MISSOURI: "MO",
  MONTANA: "MT",
  NEBRASKA: "NE",
  NEVADA: "NV",
  "NEW HAMPSHIRE": "NH",
  "NEW JERSEY": "NJ",
  "NEW MEXICO": "NM",
  "NEW YORK": "NY",
  "NORTH CAROLINA": "NC",
  "NORTH DAKOTA": "ND",
  "NORTHERN MARIANA IS": "MP",
  OHIO: "OH",
  OKLAHOMA: "OK",
  OREGON: "OR",
  PENNSYLVANIA: "PA",
  "PUERTO RICO": "PR",
  "RHODE ISLAND": "RI",
  "SOUTH CAROLINA": "SC",
  "SOUTH DAKOTA": "SD",
  TENNESSEE: "TN",
  TEXAS: "TX",
  UTAH: "UT",
  VERMONT: "VT",
  VIRGINIA: "VA",
  "VIRGIN ISLANDS": "VI",
  WASHINGTON: "WA",
  "WEST VIRGINIA": "WV",
  WISCONSIN: "WI",
  WYOMING: "WY",
};

function CompanyFilter(props) {
  let state = props.state;
  let city = props.city;
  let zip = props.zip;
  if (state === null) {
    state = "State";
  }
  if (city === null) {
    city = "City";
  }
  if (zip === null) {
    zip = "Zip";
  }
  return (
    <Container>
      <Stack
        direction="row"
        justifyContent="center"
        gap="16px"
        alignItems="center"
      >
        <FormControl sx={{ m: 1, width: 200 }}>
          <InputLabel id="demo-multiple-name-label">{state}</InputLabel>
          <Select
            labelId="select-label"
            input={<OutlinedInput label="States" />}
          >
            {Object.entries(states).map((state) => (
              <MenuItem
                onClick={() => props.update("state", state[1])}
                value={state[0]}
              >
                {state[0]}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <TextField
          onKeyPress={(ev) => {
            if (ev.key === "Enter") {
              ev.preventDefault();
              props.update("zip", ev.target.value);
            }
          }}
          label={zip}
        />
        <TextField
          onKeyPress={(ev) => {
            if (ev.key === "Enter") {
              ev.preventDefault();
              props.update("city", ev.target.value);
            }
          }}
          label={city}
        />
      </Stack>
    </Container>
  );
}

export default CompanyFilter;
