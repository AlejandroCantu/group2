import React from "react";
import { Container } from "react-bootstrap";
import "../../../styles/CountyInstance.css";
import "../../../styles/Counties.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import "../../../styles/Companies.css";

function CompanyRelations(props) {
  let company = props.company;
  return (
    <div class="model-links">
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 250 }}>
          <TableHead>
            <TableRow>
              <TableCell>
                {" "}
                <strong> Facility Information</strong>{" "}
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {company.facilities_list.map((fac) => (
              <Container>
                <p>
                  <a className="fac-button" href={"/facilities/id=" + fac.id}>
                    {" "}
                    {fac.name}{" "}
                  </a>
                </p>
              </Container>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <br></br>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 250 }}>
          <TableHead>
            <TableRow>
              <TableCell>
                {" "}
                <strong> County Information</strong>{" "}
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {company.county ? (
              <a
                className="county-button"
                href={"/counties/id=" + company.county.id}
              >
                {" "}
                {company.county.name}{" "}
              </a>
            ) : (
              "County data unavailable"
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default CompanyRelations;
