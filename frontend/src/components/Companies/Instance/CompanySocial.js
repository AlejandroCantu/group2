import Button from "@mui/material/Button";
import { Stack } from "@mui/material";

function CompanySocial(props) {
  let company = props.company;
  return (
    <div className="socialSpacer">
      <Stack flexWrap="wrap" direction="row" className="center-row" gap="64px">
        {company["website"] != null ? (
          <Button variant="outlined" href={company["website"]}>
            {" "}
            Company Website{" "}
          </Button>
        ) : null}
        {company["linked_in"] != null ? (
          <Button variant="outlined" href={company["linked_in"]}>
            {" "}
            LinkedIn{" "}
          </Button>
        ) : null}
        {company["facebook"] != null ? (
          <Button variant="outlined" href={company["facebook"]}>
            {" "}
            Facebook{" "}
          </Button>
        ) : null}
        {company["twitter"] != null ? (
          <Button
            variant="outlined"
            href={"https://twitter.com/" + company["twitter"]}
          >
            {" "}
            Twitter{" "}
          </Button>
        ) : null}
      </Stack>
    </div>
  );
}

export default CompanySocial;
