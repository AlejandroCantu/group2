function CompanyStats(props) {
  let company = props.company;
  return (
    <div>
      <h1 class="subHeader">Company Stats</h1>
      <p>Founded: {company["founded"] ? company["founded"] : "Unavailable"}</p>
      <p>Sector: {company["sector"] ? company["sector"] : "Unavailable"}</p>
      <p>
        Subsidiaries:{" "}
        {company["subsidiaries"] ? company["subsidiaries"] : "Unavailable"}
      </p>
      <p>
        Branches: {company["branches"] ? company["branches"] : "Unavailable"}
      </p>
      <p>
        Revenue: {company["revenue"] ? "$" + company["revenue"] : "Unavailable"}
      </p>
      <h1 class="subHeader">Company Contact</h1>
      <p>Phone: {company["phone"] ? company["phone"] : "Unavailable"}</p>
      <p>Fax: {company["fax"] ? company["fax"] : "Unavailable"}</p>
      <p>Email: {company["email"] ? company["email"] : "Unavailable"}</p>
    </div>
  );
}

export default CompanyStats;
