import { Card, CardContent } from "@mui/material";
import { TwitterTimelineEmbed } from "react-twitter-embed";

function CompanyTwitter(props) {
  let company = props.company;
  return (
    <Card className="twitterCard">
      <CardContent>
        {company["twitter"] ? (
          <TwitterTimelineEmbed
            sourceType="profile"
            screenName={company["twitter"] ? company["twitter"] : ""}
            options={{ height: 600 }}
          />
        ) : (
          "Twitter Not Available"
        )}
      </CardContent>
    </Card>
  );
}

export default CompanyTwitter;
