import { CardActionArea, Card, CardContent } from "@mui/material";
import { ListGroup, ListGroupItem } from "react-bootstrap";
import { Link as RouterLink } from "react-router-dom";
import defaultLogo from "../../assets/models/default_company_logo.svg";
import { React } from "react";
import { MDBCardImage } from "mdb-react-ui-kit";
import "../../styles/Card.css";
import "../../styles/Companies.css";
import Highlight from "../Highlight/Highlight";

function CompanyTile(props) {
  return (
    <Card className="company_card">
      <CardActionArea
        className="company_card_action"
        component={RouterLink}
        to={"/companies/id=" + props.c.id}
      >
        <MDBCardImage
          className="companyLogo"
          variant="top"
          src={"https://logo.clearbit.com/" + props.c.website}
          onError={({ currentTarget }) => {
            currentTarget.onerror = null;
            currentTarget.src = defaultLogo;
          }}
        />
        {
          <CardContent>
            <h1 class="cardTitle">
              <Highlight by={props.search}>{props.c.name}</Highlight>
            </h1>
            <h3 class="cardSub">
              <Highlight by={props.search}>{props.c.state}</Highlight>
            </h3>
            <CardContent>
              <ListGroup>
                <ListGroupItem>
                  <strong>Headquartered: </strong>
                  <Highlight by={props.search}>{props.c.city}</Highlight>,{" "}
                  <Highlight by={props.search}>{props.c.state}</Highlight>
                </ListGroupItem>
                <ListGroupItem>
                  <strong>Revenue: </strong>
                  <Highlight by={props.search} pre={`$`} def={"Not available"}>
                    {props.c.revenue}
                  </Highlight>
                </ListGroupItem>
                <ListGroupItem>
                  <strong>Facility Emissions: </strong>
                  <Highlight by={props.search}>{props.c.emissions}</Highlight>
                </ListGroupItem>
                <ListGroupItem>
                  <strong>Founded: </strong>
                  <Highlight by={props.search}>{props.c.founded}</Highlight>
                </ListGroupItem>
                <ListGroupItem>
                  <strong>Subsidiaries: </strong>
                  <Highlight by={props.search}>
                    {props.c.subsidiaries}
                  </Highlight>
                </ListGroupItem>
              </ListGroup>
            </CardContent>
          </CardContent>
        }
      </CardActionArea>
    </Card>
  );
}

export default CompanyTile;
