import React from "react";
import { useEffect, useState } from "react";
import { Container, Col, Row } from "react-bootstrap";
import "../../styles/CountyInstance.css";
import "../../styles/Counties.css";
import { CircularProgress } from "@mui/material";
import { useParams } from "react-router-dom";
import "../../styles/Companies.css";
import defaultLogo from "../../assets/models/default_company_logo.svg";
import CompanyStats from "./Instance/CompanyStats";
import CompanyRelations from "./Instance/CompanyRelations";
import CompanySocial from "./Instance/CompanySocial";
import CompanyTwitter from "./Instance/CompanyTwitter";

<link
  href="https://fonts.googleapis.com/css2?family=Raleway&display=swap"
  rel="stylesheet"
/>;

function CompanyInfo() {
  let id = useParams().companyId ?? "1";
  let [company, setCompany] = useState([]);
  let [loaded, setLoaded] = useState(Boolean);
  useEffect(() => {
    setLoaded(false);
    const getData = async () => {
      let response = await fetch(
        `https://api.findingfootprints.me/companies/${id}`
      );
      let body = [];
      body = await response.json();
      setCompany(body);
      setLoaded(true);
    };
    getData();
  }, [id]);

  if (!loaded) return <CircularProgress />;
  return (
    <Container>
      <Row className="Card">
        <Col>
          <h1 class="cardTitle">{company["name"]}</h1>
          <hr />
          <div style={{ display: "flex", justifyContent: "center" }}>
            <img
              className="companyMainLogo"
              src={"https://logo.clearbit.com/" + company.website}
              alt="company logo"
              onError={({ currentTarget }) => {
                currentTarget.onerror = null;
                currentTarget.src = defaultLogo;
              }}
            />
          </div>
          <hr />
        </Col>

        <div class="countyDescr">
          {" "}
          {company["description"]
            ? company["description"]
            : "No company description available"}{" "}
        </div>

        <CompanySocial company={company} />

        <Row>
          <Col>
            <CompanyStats company={company} />
          </Col>
          <Col>
            <CompanyRelations company={company} />
          </Col>
          <CompanyTwitter company={company} />
        </Row>
      </Row>
    </Container>
  );
}

export default CompanyInfo;
