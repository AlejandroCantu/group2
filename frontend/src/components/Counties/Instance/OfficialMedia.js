import {
  FacebookOutlined,
  TwitterOutlined,
  GlobalOutlined,
  PhoneOutlined,
  MailOutlined,
} from "@ant-design/icons";
import { TwitterTimelineEmbed } from "react-twitter-embed";
import Avatar from "@mui/material/Avatar";
import { ListGroupItem } from "react-bootstrap";
import { Stack } from "@mui/material";
import Democrat from "../../../assets/models/partylogos/democratic.png";
import Republican from "../../../assets/models/partylogos/republican.png";
import Independent from "../../../assets/models/partylogos/independent.png";

function valid(social, def, link) {
  if (social !== def) {
    return link;
  }
}

function avatar_party(party) {
  if (party === "Democratic Party") {
    return Democrat;
  } else if (party === "Republican Party") {
    return Republican;
  } else {
    return Independent;
  }
}

function OfficialMedia(props) {
  let fac = props.fac;
  return (
    <div>
      <ListGroupItem>
        <div class="socials">
          {valid(fac.office, "office name does not exist", fac.office) +
            ": " +
            fac.name}

          <Stack
            direction="row"
            justifyContent="right"
            alignItems="center"
            gap="8px"
          >
            {valid(
              fac.phone,
              "phone does not exist",
              <a href={"tel:" + fac.phone}>
                <PhoneOutlined />{" "}
              </a>
            )}
            {valid(
              fac.twitter,
              "twitter does not exist",
              <a href={"http://twitter.com/" + fac.twitter}>
                {" "}
                <TwitterOutlined />{" "}
              </a>
            )}
            {valid(
              fac.email,
              "email does not exist",
              <a target="" href={"mailto:" + fac.email}>
                {" "}
                <MailOutlined />{" "}
              </a>
            )}
            {valid(
              fac.facebook,
              "facebook does not exist",
              <a href={"https://www.facebook.com/" + fac.facebook}>
                {" "}
                <FacebookOutlined />{" "}
              </a>
            )}
            {valid(
              fac.website,
              "url does not exist",
              <a target="" href={fac.website}>
                {" "}
                <GlobalOutlined />{" "}
              </a>
            )}
            <Avatar
              class="avatar"
              alt={fac.party}
              src={avatar_party(fac.party)}
            />
          </Stack>
          <TwitterTimelineEmbed
            sourceType="profile"
            screenName={fac.twitter}
            options={{ height: 400 }}
          />
        </div>
      </ListGroupItem>
    </div>
  );
}

export default OfficialMedia;
