import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import Stack from "@mui/material/Stack";

//All filters listed as arrays

const states = [
  "Alabama",
  "Alaska",
  "Arizona",
  "Arkansas",
  "California",
  "Colorado",
  "Connecticut",
  "Delaware",
  "District of Columbia",
  "Florida",
  "Georgia",
  "Hawaii",
  "Idaho",
  "Illinois",
  "Indiana",
  "Iowa",
  "Kansas",
  "Kentucky",
  "Louisiana",
  "Maine",
  "Maryland",
  "Massachusetts",
  "Michigan",
  "Minnesota",
  "Mississippi",
  "Missouri",
  "Montana",
  "Nebraska",
  "Nevada",
  "New Hampshire",
  "New Jersey",
  "New Mexico",
  "New York",
  "North Carolina",
  "North Dakota",
  "Ohio",
  "Oklahoma",
  "Oregon",
  "Pennsylvania",
  "Rhode Island",
  "South Carolina",
  "South Dakota",
  "Tennessee",
  "Texas",
  "Utah",
  "Vermont",
  "Virginia",
  "Washington",
  "West Virginia",
  "Wisconsin",
  "Wyoming",
];
const income = [
  "$9,951 to $40,525",
  "$40,526 to $86,375",
  "$86,376 to $164,925",
];
const ethnicity = [
  "White (Non-Hispanic)",
  "White (Hispanic)",
  "Black or African American (Non-Hispanic)",
  "Asian (Non-Hispanic)",
  "Two+ (Non-Hispanic)",
];
const emissions = [
  "1 to 100",
  "101 to 500",
  "500 to 1,000",
  "1,000 to 10,000",
  "10,001 to 50,000",
  "50,001 to 100,000",
  "100,001 to 500,000",
  "500,001 to 999,999",
  "1,000,000 or more",
];

function CountyFilter(props) {
  let state = props.state;
  let medianIncome = props.income;
  let ethnic = props.ethnicity;
  let emiss = props.emissions;
  if (props.state === null) {
    state = "States";
  }
  if (props.income === null) {
    medianIncome = "Median Income";
  }
  if (props.ethnicity === null) {
    ethnic = "2nd Largest Ethnicity";
  }
  if (props.emissions === null) {
    emiss = "Total Emissions";
  }
  //Place filters into select forms
  return (
    //State Filter
    <Stack direction="row" justifyContent="center" gap="24px">
      <FormControl sx={{ m: 1, width: 200 }}>
        <InputLabel id="demo-multiple-name-label">{state}</InputLabel>
        <Select labelId="select-label" input={<OutlinedInput label="States" />}>
          {states.map((name) => (
            <MenuItem onClick={() => props.update("state", name)} value={name}>
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      {/* //Emissions Filter */}
      <FormControl sx={{ m: 1, width: 250 }}>
        <InputLabel id="demo-multiple-name-label">{emiss}</InputLabel>
        <Select
          labelId="select-label"
          input={<OutlinedInput label="emissions" />}
        >
          {emissions.map((name) => (
            <MenuItem
              onClick={() => props.update("emissions", name)}
              value={name}
            >
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      {/* Ethnicity Filter */}
      <FormControl sx={{ m: 1, width: 250 }}>
        <InputLabel id="demo-multiple-name-label">{ethnic}</InputLabel>
        <Select
          labelId="select-label"
          input={<OutlinedInput label="ethnicity" />}
        >
          {ethnicity.map((name) => (
            <MenuItem
              onClick={() => props.update("ethnicity", name)}
              value={name}
            >
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>

      {/* Income Filter  */}
      <FormControl sx={{ m: 1, width: 250 }}>
        <InputLabel id="demo-multiple-name-label">{medianIncome}</InputLabel>
        <Select labelId="select-label" input={<OutlinedInput label="Income" />}>
          {income.map((name) => (
            <MenuItem onClick={() => props.update("income", name)} value={name}>
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Stack>
  );
}
export default CountyFilter;
