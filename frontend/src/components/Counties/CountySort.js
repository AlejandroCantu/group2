import { Container } from "react-bootstrap";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";

//All sorts
function CountySort(props) {
  const goodName = {
    "name false": "Name: A-Z",
    "name true": "Name: Z-A",
    "emissions false": "Emissions(Asc.)",
    "emissions true": "Emissions(Desc.)",
  };
  let sorting = props.sort + " " + props.desc;
  if (props.sort === null) {
    sorting = "Sort";
  } else {
    sorting = goodName[sorting];
  }
  return (
    //Put all sorts into a select form
    <Container>
      <FormControl sx={{ m: 1, width: 150 }}>
        <InputLabel id="demo-multiple-name-label">{sorting}</InputLabel>
        <Select labelId="select-label" input={<OutlinedInput label="Sort" />}>
          <MenuItem
            onClick={() => props.update("name", "false")}
            value="Name: A-Z"
          >
            Name: A-Z
          </MenuItem>
          <MenuItem
            onClick={() => props.update("name", "true")}
            value="Name: Z-A"
          >
            Name: Z-A
          </MenuItem>
          <MenuItem
            onClick={() => props.update("emissions", "false")}
            value="Emissions(Asc.)"
          >
            Emissions(Asc.)
          </MenuItem>
          <MenuItem
            onClick={() => props.update("emissions", "true")}
            value="Emissions(Desc.)"
          >
            Emissions(Desc.)
          </MenuItem>
        </Select>
      </FormControl>
    </Container>
  );
}
export default CountySort;
