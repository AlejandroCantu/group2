import { ListGroup, ListGroupItem } from "react-bootstrap";
import { Link as RouterLink } from "react-router-dom";
import "../../styles/Card.css";
import "../../styles/Counties.css";
import { CardActionArea, Stack, Card, CardContent } from "@mui/material";
import { React } from "react";
import Highlight from "../Highlight/Highlight";
import geojson2svg from "geojson-to-svg";
import SVG from "react-inlinesvg";

//Convert SVG string to render svg
function fixString(file) {
  const geoj = JSON.parse(file);
  return geojson2svg()
    .data(geoj)
    .render()
    .toString()
    .replace("xmlns:xlink", "xmlnsXlink")
    .replace(
      'stroke="#333333" stroke-opacity="0.75" stroke-width="0.25" stroke-linecap="round" stroke-linejoin="round" fill="none"',
      'stroke="#84c28f" stroke-opacity="0.75" stroke-width="0.01" stroke-linecap="round" stroke-linejoin="round" fill="#ebfcee"'
    )
    .replace("<svg ", '<svg width="250" height="200" ');
}
function CountyImage(file) {
  const mySVG = fixString(file);
  return <SVG src={mySVG} />;
}

//County Card View
function CountyList(props) {
  return (
    <Stack justifyContent="center" direction="row" flexWrap="wrap" gap="32px">
      {props.oldList.map((c) => (
        <Card className="county_card">
          <CardActionArea
            className="county_card"
            component={RouterLink}
            to={"/counties/id=" + c.id}
          >
            <CardContent>
              <h1 class="cardTitle">
                {" "}
                <Highlight by={props.highlight}>{c.name}</Highlight>{" "}
              </h1>
              <h3 class="cardSub">
                <Highlight by={props.highlight}>{c.state}</Highlight>
              </h3>
              {CountyImage(c.img_url)}

              <ListGroup className="card-content">
                <ListGroupItem>
                  <strong>Total Emissions:</strong>{" "}
                  <Highlight by={props.highlight}>
                    {c.emissions.toLocaleString()}
                  </Highlight>{" "}
                  CO2e
                </ListGroupItem>

                <ListGroupItem>
                  <strong>Median Household: </strong>{" "}
                  <Highlight by={props.highlight} pre={`$`}>
                    {"$" + c.income.toLocaleString()}
                  </Highlight>
                </ListGroupItem>
                <ListGroupItem>
                  <strong>Population:</strong>{" "}
                  <Highlight by={props.highlight}>
                    {c.population.toLocaleString()}
                  </Highlight>
                </ListGroupItem>
                <ListGroupItem>
                  <strong>Second Largest Ethnic Group:</strong>{" "}
                  <p>
                    <Highlight by={props.highlight}>{c.ethnicity}</Highlight>
                  </p>
                </ListGroupItem>
                <ListGroupItem>
                  <strong>Officials: </strong>{" "}
                  <Highlight by={props.highlight}>{c.numOfficials}</Highlight>
                </ListGroupItem>
              </ListGroup>
            </CardContent>
          </CardActionArea>
        </Card>
      ))}
    </Stack>
  );
}

export default CountyList;
