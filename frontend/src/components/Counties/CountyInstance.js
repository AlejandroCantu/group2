import React from "react";
import { useState, useEffect } from "react";
import { Container, Col, Row } from "react-bootstrap";
import "../../styles/CountyInstance.css";
import "../../styles/Counties.css";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { useParams, Link } from "react-router-dom";
import "../../index.css";
import { CircularProgress } from "@mui/material";
import OfficialMedia from "./Instance/OfficialMedia";

function CountyInstance(props) {
  //Retreive counties data from api
  let [loaded, setLoaded] = useState(Boolean);
  let id = useParams().countyId ?? "1";
  let [county, setCounty] = useState([]);
  useEffect(() => {
    setLoaded(false);
    const getData = async () => {
      let response = await fetch(
        `https://api.findingfootprints.me/counties/${id}`
      );
      let body = [];
      body = await response.json();
      setCounty(body);
      setLoaded(true);
    };
    getData();
  }, [id]);

  if (!loaded) {
    return <CircularProgress />;
  }
  //Depict all county attributes on page
  return (
    <Container>
      <Row className="Card">
        <Col>
          <h1 class="cardTitle">{county["name"]}</h1>
          <h2 class="cardSub">{county["state"]}</h2>
          {county["name"] !== undefined && (
            <iframe
              class="frameMap"
              title="map"
              width="450"
              height="250"
              frameBorder="10"
              style={{ border: 0 }}
              referrerPolicy="no-referrer-when-downgrade"
              src={
                "https://www.google.com/maps/embed/v1/place?key=AIzaSyBT4ozQAMfFvsK7RkCwClt07qhhob2iqDY&q=" +
                encodeURIComponent(county["name"]) +
                county["state"].replace(/ /g, "+")
              }
              allowFullScreen
            ></iframe>
          )}

          <hr />
          <strong class="countyDescr">County Description:</strong>
          <h class="countyDescr"> {county["info"]} </h>

          <hr />
          <strong class="countyDescr">Demographic Distribution:</strong>
          <h class="countyDescr"> {county["demographic"]} </h>

          <hr />
        </Col>

        <Row>
          <Col>
            <div class="bodyText">
              <p>
                {" "}
                <strong>Population:</strong>{" "}
                {county["population"].toLocaleString()}{" "}
              </p>
              <p>
                <strong>County Officials:</strong>

                {county["officials"]?.map((fac) => (
                  <OfficialMedia fac={fac} />
                ))}
              </p>
            </div>
          </Col>

          <Col>
            <div class="model-links">
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 250 }}>
                  <TableHead>
                    <TableRow>
                      <TableCell>
                        {" "}
                        <strong class="bodyText">
                          {" "}
                          Companies Information
                        </strong>{" "}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {county["companies"]?.map((c) => (
                      <Link class="link" to={"/companies/id=" + c.id}>
                        <p class="fac-button">{c.name}</p>
                      </Link>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <br></br>
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 250 }}>
                  <TableHead>
                    <TableRow>
                      <TableCell>
                        {" "}
                        <strong class="bodyText">
                          {" "}
                          Facility Information
                        </strong>{" "}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {county["facilities"]?.map((fa) => (
                      <Link class="link" to={"/facilities/id=" + fa.id}>
                        <p className="fac-button"> {fa.name} </p>
                      </Link>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          </Col>
        </Row>
      </Row>
    </Container>
  );
}

export default CountyInstance;
