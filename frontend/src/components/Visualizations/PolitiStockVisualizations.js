import React from 'react';
import { useEffect, useState } from "react";
import { CircularProgress } from "@mui/material";
import '@grapecity/wijmo.styles/wijmo.css';
import BubbleChart from "./react-bubble-chart-d3"


const StocksMarketVisualization = () => {
    const [data] = useState([])
    const [loaded, setLoaded] = useState(Boolean)

    useEffect(() => {
        
        const getData = async () => {
            if(data === undefined || data.length === 0){
                let url = `https://api.politistock.me/stocks?sort=marketCap&order=desc`
                let response = await fetch (url);
                let body = []
                body = await response.json()
                var i  = 0 
            
                for (const item in body) {
                    let stockName = body[item]["id"];
                    let stockValue = body[item]["marketCap"];
                    data.push({ label: stockName, value: stockValue})
                    i++;
                    if(i === 80){
                        break;
                    }
                    
                }
                    
                setLoaded(true)
            }
        };
        getData();
    },[data]);
    
    if(!loaded) {
        return <CircularProgress />
    }

    return (
        <BubbleChart
            graph={{
                zoom: 0.7
            }}
            showLegend={true}
            width={1000}
            height={800}
            valueFont={{
                family: "Arial",
                size: 12,
                color: "#fff",
                weight: "bold",
            }}
            labelFont={{
                family: "Arial",
                size: 16,
                color: "#fff",
                weight: "bold",
            }}
            data={data}
        /> 
    )

}




export {StocksMarketVisualization}