import React from 'react';
import { PureComponent, useEffect, useState } from "react";
import { CircularProgress, Stack } from "@mui/material";
import { 
    Treemap
} from "recharts";
import "../../styles/Visualizations.css";

let states = {
    ALABAMA: 0,
    ALASKA: 1,
    ARIZONA: 2,
    ARKANSAS: 3,
    CALIFORNIA: 4,
    COLORADO: 5,
    CONNECTICUT: 6,
    DELAWARE: 7,
    "DISTRICT OF COLUMBIA": 8,
    FLORIDA: 9,
    GEORGIA: 10,
    HAWAII: 11,
    IDAHO: 12,
    ILLINOIS: 13,
    INDIANA: 14,
    IOWA: 15,
    KANSAS: 16,
    KENTUCKY: 17,
    LOUISIANA: 18,
    MAINE: 19,
    MARYLAND: 20,
    MASSACHUSETTS: 21,
    MICHIGAN: 22,
    MINNESOTA: 23,
    MISSISSIPPI: 24,
    MISSOURI: 25,
    MONTANA: 26,
    NEBRASKA: 27,
    NEVADA: 28,
    "NEW HAMPSHIRE": 29,
    "NEW JERSEY": 30,
    "NEW MEXICO": 31,
    "NEW YORK": 32,
    "NORTH CAROLINA": 33,
    "NORTH DAKOTA": 34,
    OHIO: 35,
    OKLAHOMA: 36,
    OREGON: 37,
    PENNSYLVANIA: 38,
    "RHODE ISLAND": 39,
    "SOUTH CAROLINA": 40,
    "SOUTH DAKOTA": 41,
    TENNESSEE: 42,
    TEXAS: 43,
    UTAH: 44,
    VERMONT: 45,
    VIRGINIA: 46,
    WASHINGTON: 47,
    "WEST VIRGINIA": 48,
    WISCONSIN: 49,
    WYOMING: 50,
  };

export default function CountyEmissionsChart() {
    let [data] = useState(
        [
        {name:"Alabama", children:[]}, {name:"Alaska", children:[]}, {name:"Arizona", children:[]}, {name:"Arkansas", children:[]},
        {name:"California", children:[]}, {name:"Colorado", children:[]}, {name:"Connecticut", children:[]}, {name:"Delaware", children:[]},
        {name:"District of Columbia", children:[]}, {name:"Florida", children:[]}, {name:"Georgia", children:[]}, {name:"Hawaii", children:[]},
        {name:"Idaho", children:[]}, {name:"Illinois", children:[]}, {name:"Indiana", children:[]}, {name:"Iowa", children:[]}, {name:"Kansas", children:[]},
        {name:"Kentucky", children:[]}, {name:"Louisiana", children:[]}, {name:"Maine", children:[]}, {name:"Maryland", children:[]},
        {name:"Massachusetts", children:[]}, {name:"Michigan", children:[]}, {name:"Minnesota", children:[]}, {name:"Mississippi", children:[]},
        {name:"Missouri", children:[]}, {name:"Montana", children:[]}, {name:"Nebraska", children:[]}, {name:"Nevada", children:[]},
        {name:"New Hampshire", children:[]}, {name:"New Jersey", children:[]}, {name:"New Mexico", children:[]}, {name:"New York", children:[]},
        {name:"North Carolina", children:[]}, {name:"North Dakota", children:[]}, {name:"Ohio", children:[]}, {name:"Oklahoma", children:[]},
        {name:"Oregon", children:[]}, {name:"Pennsylvania", children:[]}, {name:"Rhode Island", children:[]}, {name:"South Carolina", children:[]},
        {name:"South Dakota", children:[]}, {name:"Tennessee", children:[]}, {name:"Texas", children:[]}, {name:"Utah", children:[]},
        {name:"Vermont", children:[]}, {name:"Virginia", children:[]}, {name:"Washington", children:[]}, {name:"West Virginia", children:[]},
        {name:"Wisconsin", children:[]}, {name:"Wyoming", children:[]}
        ],)
    const [loaded, setLoaded] = useState(Boolean)

    useEffect(() => {
        const constructDataset = (counties) => {
            const dataset = counties.map((county) => {
                data[states[county.state.toUpperCase()]].children.push({name:county.name, size:county.emissions})
                return null
            })
            return dataset
        }
        
        const getData = async () => {
                let url = `https://api.findingfootprints.me/counties?page=1&per_page=35&sort=emissions&desc=true`
                let response = await fetch (url);
                let body = []
                body = await response.json()
                constructDataset(body['list'])
                setLoaded(true)
        };
        getData();
    }, [data]);

    const COLORS = ['#8889DD', '#9597E4', '#8DC77B', '#A5D297', '#E2CF45', '#F8C12D'];

    class CustomizedContent extends PureComponent {
      render() {
        const { root, depth, x, y, width, height, index, colors, name } = this.props;
        return (
          <g>
            <rect
              x={x}
              y={y}
              width={width}
              height={height}
              style={{
                fill: depth < 2 ? colors[Math.floor((index / root.children.length) * 6)] : 'none',
                stroke: '#fff',
                strokeWidth: 2 / (depth + 1e-10),
                strokeOpacity: 1 / (depth + 1e-10),
              }}
            />
            {depth === 1 ? (
              <text x={x + width / 2} y={y + height / 2 + 7} textAnchor="middle" fill="#fff" fontSize={14}>
                {name}
              </text>
            ) : null}
            {depth === 1 ? (
              <text x={x + 4} y={y + 18} fill="#fff" fontSize={16} fillOpacity={0.9}>
                {index + 1}
              </text>
            ) : null}
          </g>
        );
      }
    }

    let filtered  = data.filter(function(value, index, arr){
        return value.children.length > 0
    })

    if(!loaded){
      return <CircularProgress />
    }
      return (
        <>
        <Stack direction="row" justifyContent="center" flexWrap="wrap" alignItems="center">
        <Treemap
          className="mapPadding"
          width={1350}
          height={700}
          data={filtered}
          dataKey="size"
          ratio={4 / 3}
          stroke="#fff"
          fill="#8884d8"
          content={<CustomizedContent colors={COLORS} />}
        />
        </Stack>
        </>
    )
}