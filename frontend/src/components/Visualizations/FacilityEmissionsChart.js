import React from 'react';
import { useEffect, useState } from "react";
import { CircularProgress, Stack } from "@mui/material";
import { 
    Line,
    LineChart,
    CartesianGrid,
    XAxis,
    YAxis,
    Tooltip,
    Label,
} from "recharts";
import "../../styles/Visualizations.css";

export default function FacilityEmissionsChart() {
    let [data] = useState([{name:"2010"}, {name:"2011"}, {name:"2012"}, {name:"2013"}, {name:"2014"}, {name:"2015"}, {name:"2016"}, {name:"2017"}, {name:"2018"}, {name:"2019"}, {name:"2020"}])
    const [facNames] = useState([])
    const [loaded, setLoaded] = useState(Boolean)

    useEffect(() => {
        const constructDataset = (facilities) => {
            const dataset = facilities.map((fac) => {
                facNames.push(fac.name)
                for(let i = 0; i < data.length; i++){
                    let year = "m1"+ i
                    data[i][fac.name] = fac[year]
                }
                data[10][fac.name] = fac["emissions"]
                return { label: fac.name, value: fac.emissions }
            })
            return dataset
        }
        
        const getData = async () => {
                let url = `https://api.findingfootprints.me/facilities?per_page=20&sort=emissions&desc=true`
                let response = await fetch (url);
                let body = []
                body = await response.json()
                constructDataset(body['list'])
                setLoaded(true)
        };
        getData();
    }, [facNames, data]);

    if(!loaded){
      return <CircularProgress />
    }
      return (
        <>
        <Stack direction="row" justifyContent="center" flexWrap="wrap" alignItems="center">
        <LineChart
          width={1200}
          height={800}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 160,
              bottom: 100,
            }}
            >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name">
              <Label value="Year" position="bottom" offset={20}/>
          </XAxis>
          <YAxis>
            <Label className="labelWrap" value="Emissions (in tons)" position="left" offset={20}/>
          </YAxis>
          <Tooltip />
          {facNames.map((fac) => {
            const randomColor = "#" + Math.floor(Math.random()*16777215).toString(16)
            return (<Line type="monotone" dataKey={fac} stroke={randomColor} />)
          }) }
        </LineChart>
        </Stack>
        </>
    )
}