import React from 'react';
import { useEffect, useState } from "react";
import { CircularProgress, Stack } from "@mui/material";
import { useNavigate } from 'react-router-dom';
import { 
        Bar,
        BarChart,
        Legend,
} from "recharts";
import BubbleChart from "./react-bubble-chart-d3";
import "../../styles/Visualizations.css";

const colorMap = {
    "Minerals": "#6B3E2E",
    "Power Plants": "#187BCD",
    "Petroleum and Natural Gas Systems": "#000000",
    "Pulp and Paper": "#E1C699",
    "Waste": "#596300",
    "Chemicals": "#FA9C1B",
    "Metals": "#D8D8D8",
    "Other": "#735C99"
}

export default function CompanyEmissionsChart() {
    let navigate = useNavigate()
    const [data, setData] = useState([])
    const [cID] = useState(new Map())
    const [loaded, setLoaded] = useState(Boolean)

    useEffect(() => {
        const constructDataset = (companies) => {
            const dataset = companies.map((company) => {
                cID.set(company.name.split(" ")[0], company.id)
                return { label: company.name.split(" ")[0], value: company.emissions, color: colorMap[company.sector] }
            })
            return dataset
        }
        
        const getData = async () => {
            if(data === undefined || data.length === 0){
                let url = `https://api.findingfootprints.me/companies?page=1&per_page=30&sort=emissions&descending=true`
                let response = await fetch (url);
                let body = []
                body = await response.json()
                const d = constructDataset(body['list'])
                setData(d)
                setLoaded(true)
            }
        };
        getData();
    }, [cID, data]);
    
    if(!loaded) {
        return <CircularProgress />
    }

    return (
        <>
        <Stack direction="row" justifyContent="center" flexWrap="wrap" alignItems="center">
        <BubbleChart
            graph={{
                zoom: 0.7
            }}
            showLegend={false}
            width={1000}
            height={800}
            valueFont={{
                family: "Arial",
                size: 11,
                color: "#fff",
                weight: "bold",
            }}
            labelFont={{
                family: "Arial",
                size: 14,
                color: "#fff",
                weight: "bold",
            }}
            data={data}
            bubbleClickFun={(val) => {
                navigate(`/companies/id=${cID.get(val)}`)
            }}
            />
        <BarChart
          width={200}
          height={5}
          data={colorMap}
          margin={{
              top: 30,
              right: 30,
              left: 20,
              bottom: 5,
            }}
            >
        <Bar dataKey={"Minerals"} fill={"#6B3E2E"} />
        <Bar dataKey={"Power Plants"} fill={"#187BCD"} />
        <Bar dataKey={"Petroleum and Natural Gas Systems"} fill={"#000000"} />
        <Bar dataKey={"Pulp and Paper"} fill={"#E1C699"} />
        <Bar dataKey={"Waste"} fill={"#596300"} />
        <Bar dataKey={"Chemicals"} fill={"#FA9C1B"} />
        <Bar dataKey={"Metals"} fill={"#D8D8D8"} />
        <Bar dataKey={"Other"} fill={"#735C99"} />
        <Legend/>
        </BarChart> 
        </Stack>
        </>
    )

}

