import React from 'react';
import { useEffect, useState } from "react";
import { CircularProgress, Stack } from "@mui/material";
import { useNavigate } from 'react-router-dom';
import { 
        Bar,
        BarChart,
        Line,
        Legend,
        LineChart,
        CartesianGrid,
        XAxis,
        YAxis,
        Tooltip,
        Label,
} from "recharts";
import BubbleChart from "./react-bubble-chart-d3";
import "../../styles/Visualizations.css";


const colorMap = {
    "Minerals": "#6B3E2E",
    "Power Plants": "#187BCD",
    "Petroleum and Natural Gas Systems": "#000000",
    "Pulp and Paper": "#E1C699",
    "Waste": "#596300",
    "Chemicals": "#FA9C1B",
    "Metals": "#D8D8D8",
    "Other": "#735C99"
}

const CompanyEmissionsChart = () => {
    let navigate = useNavigate()
    const [data, setData] = useState([])
    const [cID] = useState(new Map())
    const [loaded, setLoaded] = useState(Boolean)

    useEffect(() => {
        const constructDataset = (companies) => {
            const dataset = companies.map((company) => {
                cID.set(company.name.split(" ")[0], company.id)
                return { label: company.name.split(" ")[0], value: company.emissions, color: colorMap[company.sector] }
            })
            return dataset
        }
        
        const getData = async () => {
            if(data === undefined || data.length === 0){
                let url = `https://api.findingfootprints.me/companies?page=1&per_page=30&sort=emissions&descending=true`
                let response = await fetch (url);
                let body = []
                body = await response.json()
                const d = constructDataset(body['list'])
                setData(d)
                setLoaded(true)
            }
        };
        getData();
    }, [cID, data]);
    
    if(!loaded) {
        return <CircularProgress />
    }

    return (
        <>
        <Stack direction="row" justifyContent="center" flexWrap="wrap" alignItems="center">
        <BubbleChart
            graph={{
                zoom: 0.7
            }}
            showLegend={false}
            width={1000}
            height={800}
            valueFont={{
                family: "Arial",
                size: 11,
                color: "#fff",
                weight: "bold",
            }}
            labelFont={{
                family: "Arial",
                size: 14,
                color: "#fff",
                weight: "bold",
            }}
            data={data}
            bubbleClickFun={(val) => {
                navigate(`/companies/id=${cID.get(val)}`)
            }}
            />
        <BarChart
          width={200}
          height={5}
          data={colorMap}
          margin={{
              top: 30,
              right: 30,
              left: 20,
              bottom: 5,
            }}
            >
        <Bar dataKey={"Minerals"} fill={"#6B3E2E"} />
        <Bar dataKey={"Power Plants"} fill={"#187BCD"} />
        <Bar dataKey={"Petroleum and Natural Gas Systems"} fill={"#000000"} />
        <Bar dataKey={"Pulp and Paper"} fill={"#E1C699"} />
        <Bar dataKey={"Waste"} fill={"#596300"} />
        <Bar dataKey={"Chemicals"} fill={"#FA9C1B"} />
        <Bar dataKey={"Metals"} fill={"#D8D8D8"} />
        <Bar dataKey={"Other"} fill={"#735C99"} />
        <Legend/>
        </BarChart> 
        </Stack>
        </>
    )

}

function FacilityEmissionsChart() {
    let [data] = useState([{name:"2010"}, {name:"2011"}, {name:"2012"}, {name:"2013"}, {name:"2014"}, {name:"2015"}, {name:"2016"}, {name:"2017"}, {name:"2018"}, {name:"2019"}, {name:"2020"}])
    const [facNames] = useState([])
    const [loaded, setLoaded] = useState(Boolean)

    useEffect(() => {
        const constructDataset = (facilities) => {
            const dataset = facilities.map((fac) => {
                facNames.push(fac.name)
                for(let i = 0; i < data.length; i++){
                    let year = "m1"+ i
                    data[i][fac.name] = fac[year]
                }
                data[10][fac.name] = fac["emissions"]
                return { label: fac.name, value: fac.emissions }
            })
            return dataset
        }
        
        const getData = async () => {
                let url = `https://api.findingfootprints.me/facilities?per_page=20&sort=emissions&desc=true`
                let response = await fetch (url);
                let body = []
                body = await response.json()
                constructDataset(body['list'])
                setLoaded(true)
        };
        getData();
    }, [facNames, data]);

    if(!loaded){
      return <CircularProgress />
    }
      return (
        <>
        <Stack direction="row" justifyContent="center" flexWrap="wrap" alignItems="center">
        <LineChart
          width={1200}
          height={800}
          data={data}
          margin={{
            top: 5,
            right: 30,
            left: 160,
              bottom: 100,
            }}
            >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name">
              <Label value="Year" position="bottom" offset={20}/>
          </XAxis>
          <YAxis>
            <Label className="labelWrap" value="Emissions (in tons)" position="left" offset={20}/>
          </YAxis>
          <Tooltip />
          {facNames.map((fac) => {
            const randomColor = "#" + Math.floor(Math.random()*16777215).toString(16)
            return (<Line type="monotone" dataKey={fac} stroke={randomColor} />)
          }) }
        </LineChart>
        </Stack>
        </>
    )
   
}


export {CompanyEmissionsChart, FacilityEmissionsChart}
