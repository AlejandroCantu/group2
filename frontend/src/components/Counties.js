import React from "react";
import { ReactComponent as CountiesMap } from "../assets/counties/countiesMap.svg";
import "../styles/Counties.css";
import CountyList from "./Counties/CountyList.js";
import { Container, Row } from "react-bootstrap";
import CountySort from "./Counties/CountySort.js";
import CountyFilter from "./Counties/CountyFilter.js";
import { useEffect, useState } from "react";
import {
  Stack,
  Pagination,
  PaginationItem,
  CircularProgress,
  TextField,
  Button,
} from "@mui/material";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import PerPagePicker from "./PerPagePicker";

function getPath({
  page = 1,
  per_page = 12,
  query,
  sort,
  state,
  emissions,
  ethnicity,
  income,
  desc,
} = {}) {
  let path = `?page=${page}&per_page=${per_page}`;
  if (query) {
    path += `&q=${query}`;
  }
  if (sort) {
    path += `&sort=${sort}`;
  }
  if (desc) {
    path += `&desc=${desc}`;
  }
  if (state) {
    path += `&state=${state}`;
  }
  if (emissions) {
    path += `&emissions=${emissions}`;
  }
  if (ethnicity) {
    path += `&ethnicity=${ethnicity}`;
  }
  if (income) {
    path += `&income=${income}`;
  }
  return path;
}

function Counties() {
  //Filters, sorts, and queries stored from the user 
  let [searchParams] = useSearchParams();
  let page = parseInt(searchParams.get("page") ?? "1");
  let per_page = parseInt(searchParams.get("per_page") ?? "9");
  let query = searchParams.get("q");
  const [searchQ, setSearch] = useState("");
  let sort = searchParams.get("sort");
  let state = searchParams.get("state");
  let emissions = searchParams.get("emissions");
  let income = searchParams.get("income");
  let ethnicity = searchParams.get("ethnicity");
  let desc = searchParams.get("desc");
  let [counties, setCounties] = useState([]);
  let [numInstances, setInstances] = useState(0);
  let [pageCount, setPageCount] = useState(0);
  let [startIndex, setStart] = useState(0);
  let [endIndex, setEnd] = useState(0);
  let [loaded, setLoaded] = useState(Boolean);
  let navigate = useNavigate();

  const clearAll = () => {
    let path = ``;
    navigate(path);
  };

  //when search update query 
  const updateSearch = (search) => {
    let path = getPath({
      page: 1,
      per_page: per_page,
      query: search,
      sort: sort,
      state: state,
      emissions: emissions,
      ethnicity: ethnicity,
      income: income,
      desc: desc,
    });
    navigate(path);
  };

  //update sort filters on click 
  const updateSort = (sort, desc) => {
    let path = getPath({
      page: page,
      per_page: per_page,
      query: query,
      sort: sort,
      state: state,
      emissions: emissions,
      ethnicity: ethnicity,
      income: income,
      desc: desc,
    });

    navigate(path);
  };

  //update num of instances per page
  const updatePerPage = (num) => {
    let path = getPath({
      page: page,
      per_page: num,
      query: query,
      sort: sort,
      state: state,
      emissions: emissions,
      ethnicity: ethnicity,
      income: income,
      desc: desc,
    });
    navigate(path);
  };

  //update filter with new filter 
  const updateFilter = (filter, what) => {
    if (filter === "state") {
      state = what;
    } else if (filter === "income") {
      income = what;
    } else if (filter === "ethnicity") {
      ethnicity = what;
    } else if (filter === "emissions") {
      emissions = what;
    }
    let path =
      getPath({
        page: 1,
        per_page: per_page,
        query: query,
        sort: sort,
        state: state,
        emissions: emissions,
        ethnicity: ethnicity,
        income: income,
        desc: desc,
      });
    navigate(path);
  };

  useEffect(() => {
    //base url joined with query, sort, and filter 
    const getData = async () => {
      setLoaded(false);
      let url = `https://api.findingfootprints.me/counties?page=${page}&per_page=${per_page}`;
      if (query) {
        url += `&q=${query}`;
      }
      if (desc) {
        url += `&desc=${desc}`;
      }
      if (sort) {
        url += `&sort=${sort}`;
      }
      if (emissions) {
        url += `&emissions=${emissions}`;
      }
      if (state) {
        url += `&state=${state}`;
      }
      if (income) {
        url += `&income=${income}`;
      }
      if (ethnicity) {
        url += `&ethnicity=${ethnicity}`;
      }
      let response = await fetch(url);
      let body = [];
      body = await response.json();
      setInstances(body["numInstances"]);
      setCounties(body["list"]);
      setPageCount(body["pageCnt"]);
      setStart(body["startIdx"]);
      setEnd(body["endIdx"]);
      setLoaded(true);
    };
    getData();
  }, [page, per_page, query, sort, desc, income, state, ethnicity, emissions]);

  if (!loaded) {
    return <CircularProgress />;
  }
  return (
    <Container>
      <h1 className="wrapper">Counties</h1>
      <Row>
        <CountiesMap />
      </Row>
      <Row>
        <h2 className="wrapper">Search</h2>
        <TextField
          onKeyPress={(ev) => {
            if (ev.key === "Enter") {
              ev.preventDefault();
              updateSearch(ev.target.value);
            }
          }}
          label="Search"
          value={searchQ}
          onChange={(event) => setSearch(event.target.value)}
        ></TextField>
      </Row>

      <Row>
        <h2 className="wrapper">Filters and Sorts</h2>
        <Stack
          alignItems="center"
          justifyContent="center"
          direction="row"
          flexWrap="wrap"
        >
          <CountyFilter
            update={updateFilter}
            state={state}
            income={income}
            ethnicity={ethnicity}
            emissions={emissions}
          />
        </Stack>
        <Stack alignItems="center" justifyContent="center" direction="row">
          <CountySort
            className="buttons"
            update={updateSort}
            sort={sort}
            desc={desc}
          />
          <PerPagePicker
            className="buttons"
            update={updatePerPage}
            num={per_page}
          />
          <Container>
            <Button
              className="buttons"
              variant="outlined"
              style={{ width: "200px", height: "50px" }}
              onClick={() => clearAll()}
            >
              Clear
            </Button>
          </Container>
        </Stack>
      </Row>
      <Row>
        <div style={{ justifyContent: "center" }}>
          <CountyList oldList={counties} highlight={query} />
        </div>
        <Row>
          <h3 className="text-center mt-5">
            Viewing Counties {startIndex} - {Math.min(numInstances, endIndex)}{" "}
            of {numInstances}
          </h3>
        </Row>
      </Row>
      <Row>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "32px",
            marginBottom: "32px",
          }}
        >
          <Pagination
            justifyContent="center"
            showFirstButton="true"
            showLastButton="true"
            shape="rounded"
            count={pageCount}
            defaultPage={page}
            renderItem={(item) => (
              <PaginationItem
                component={Link}
                to={`?page=${item.page}&per_page=${per_page}`}
                {...item}
              />
            )}
          />
        </div>
      </Row>
    </Container>
  );
}
export default Counties;
