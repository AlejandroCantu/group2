import { Container } from "react-bootstrap";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";

const options = ["9", "18", "36"];

function PerPagePicker(props) {
  let num = props.num + " Per Page"
  return (
    <Container>
      <FormControl sx={{ m: 1, width: 150 }}>
        <InputLabel id="demo-multiple-name-label">{num}</InputLabel>
        <Select
          labelId="select-label"
          input={<OutlinedInput label="Per Page" />}
        >
          {options.map((num) => (
            <MenuItem onClick={() => props.update(num)} value={num}>
              {num}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Container>
  );
}
export default PerPagePicker;
