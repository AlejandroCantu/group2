import React from "react";
import styles from "./HomePage.module.css";
import {
  Stack,
  Button,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Typography,
  TextField,
} from "@mui/material";
import companyImg from "../../assets/models/company_splash.jpg";
import countyImg from "../../assets/models/USA_map.jpg";
import facilityImg from "../../assets/models/facility_splash.jpg";
import { useNavigate } from "react-router-dom";
import "../../styles/Splash.css";
import "../../styles/Card.css";

const Home = () => {
  let navigate = useNavigate();

  const updateSearch = (search) => {
    let path = `search?q=${search}`;
    navigate(path);
  };

  return (
    <div>
      <div className={styles.homePage}>
        <div className={styles.greenBlob} />
        <div className={styles.blueBlob} />

        <div className={styles.homeTitle}>
          Finding Footprints
          <div className={styles.homeSubTitle}>
            Cleaner World, Happier World.
            <div className={styles.homeSubSubTitle}>
              Hold companies and facilities responsible for their actions.{" "}
              <br></br>
              Find the greatest contributors to pollution in your area today.
            </div>
          </div>
          <TextField
            className="searchbar"
            onKeyPress={(ev) => {
              if (ev.key === "Enter") {
                ev.preventDefault();
                updateSearch(ev.target.value);
              }
            }}
            label="Search"
            placeholder="Enter sitewide search here"
          />
          <div className={styles.spacer}></div>
          <div className={styles.homeSubTitle}>
            <h3>
              In 2019, the United States emitted 6,558,345 thousand tons of CO2.
            </h3>
            <h3>
              That is 42% of the combined emissions of all{" "}
              <Button
                variant="outlined"
                href="https://www.oecd.org/"
                style={{
                  color: "whitesmoke",
                  fontSize: "16px",
                  fontStyle: "bold",
                }}
              >
                OECD
              </Button>{" "}
              member nations.
            </h3>
          </div>
        </div>
      </div>
      <h1 className="wrapper"> Models </h1>
      <Stack direction="row" flexWrap={true} className="center-row">
        <Card className="modelCard" sx={{ width: 345 }}>
          <CardActionArea href="/companies">
            <CardMedia height="200" component="img" image={companyImg} />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Companies
              </Typography>
              <Typography variant="body2" color="text.secondary">
                View all companies
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>

        <Card className="modelCard" sx={{ width: 345 }}>
          <CardActionArea href="/facilities">
            <CardMedia height="200" component="img" image={facilityImg} />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Facilities
              </Typography>
              <Typography variant="body2" color="text.secondary">
                View all facilities
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>

        <Card className="modelCard" sx={{ width: 345 }}>
          <CardActionArea href="/counties">
            <CardMedia height="200" component="img" image={countyImg} />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                Counties
              </Typography>
              <Typography variant="body2" color="text.secondary">
                View all counties
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Stack>
      <h1 className="wrapper"> Presentation </h1>
      <iframe width="840" height="472" src="https://www.youtube.com/embed/qDLs6qO0b6o" title="YouTube video player" frameborder="0" allow="accelerometer; fullscreen; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
      <div className="videoWrapper"></div>
    </div>
  );
};
export default Home;
