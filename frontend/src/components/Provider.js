import { React } from "react";
import { Container } from "react-bootstrap";
import PoliticianActivityChart from "./Provider/PoliticianActivityChart"
import CompanySectorsChart from "./Provider/CompanySectorsChart.js"
import StockMarketChart from "./Provider/StockMarketChart";
import { Stack } from "@mui/material"
import "../styles/Visualizations.css"

function Provider() {

  return (
    <Container>
    <h1 className="wrapper">Provider Visualizations</h1>

    <Stack>
    <h1 className="wrapperTitle">Companies by Industry</h1>
    <CompanySectorsChart /> 

    <h1 className="wrapperTitle">Stocks by Market Cap</h1>
    <StockMarketChart/>

    <h1 className="wrapperTitle">Politician Activity by Chamber and Party</h1>
    <PoliticianActivityChart/>
    </Stack>
  
    </Container>
  );

}
export default Provider;
