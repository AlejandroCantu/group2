// Bio pictures
import DavidTPic from "../../assets/profilepics/David-profilepic.jpg";
import AlejandroCPic from "../../assets/profilepics/Alejandro-profilepic.jpg";
import VibhutiSPic from "../../assets/profilepics/Vibhuti-profilepic.jpg";
import GriffinUPic from "../../assets/profilepics/Griffin-profilepic.jpg";

// Tool Logos
import ReactLogo from "../../assets/tools/ReactLogo.png";
import BootstrapLogo from "../../assets/tools/ReactBootstrapLogo.png";
import MaterialLogo from "../../assets/tools/MaterialUILogo.png";
import GitLabLogo from "../../assets/tools/GitLabLogo.png";
import PostmanLogo from "../../assets/tools/PostmanLogo.png";
import NamecheapLogo from "../../assets/tools/NamecheapLogo.png";
import AWSLogo from "../../assets/tools/AWSLogo.png";
import AmazonRDSLogo from "../../assets/tools/AmazonRDS.png";
import DiscordLogo from "../../assets/tools/DiscordLogo.png";
import JestLogo from "../../assets/tools/JestLogo.png";
import SeleniumLogo from "../../assets/tools/SeleniumLogo.png";
import DockerLogo from "../../assets/tools/DockerLogo.png";
import FlaskLogo from "../../assets/tools/FlaskLogo.png";
import SQLalchemyLogo from "../../assets/tools/SQLalchemyLogo.jpg";
import PostgresqlLogo from "../../assets/tools/PostgresqlLogo.png";
import EBLogo from "../../assets/tools/EBLogo.png";
import PrettierLogo from "../../assets/tools/prettierLogo.svg";
import BlackLogo from "../../assets/tools/BlackLogo.png";
import D3Logo from "../../assets/tools/D3Logo.png";
import RechartsLogo from "../../assets/tools/RechartsLogo.png";

// API logos
import EPALogo from "../../assets/apis/EPALogo.png";
import EIALogo from "../../assets/apis/EIALogo.png";
import ClearBitLogo from "../../assets/apis/ClearBitLogo.png";
import GMapsLogo from "../../assets/apis/GoogleMapsLogo.png";
import GCivicsLogo from "../../assets/apis/GoogleCivicsLogo.jpg";
import OrbIntelLogo from "../../assets/apis/OrbIntelligenceLogo.png";

const memberInfo = [
  {
    Name: "Alejandro Cantu",
    Username: "AlejandroCantu",
    Email: "alejandro.cantu1018@gmail.edu",
    Role: "Back-end | Phase II Lead",
    Photo: AlejandroCPic,
    Bio: "Hey y'all! My name is Alejandro Cantu and I am a junior at UT Austin majoring in computer science and minoring in philosophy of law. In my free time I love to watch and play basketball, mountain bike, hike, fish, play video games, and watch movies.",
    Commits: 0,
    Issues: 0,
    Tests: 15,
  },
  {
    Name: "Vibhuti Sharma",
    Username: "vibhutisharma",
    Email: "sharma.vibhuti@utexas.edu",
    Role: "Full Stack | Phase III Lead",
    Photo: VibhutiSPic,
    Bio: "I am a second-year CS major at UT Austin. I grew up in 7 different states but went to high school in a suburb near Houston, TX. In my free time, I enjoy doing outdoor activities like hiking and kayaking and curating Spotify playlists.",
    Commits: 0,
    Issues: 0,
    Tests: 6,
  },
  {
    Name: "David Tang",
    Username: "davtang",
    Email: "davtang@utexas.edu",
    Role: "Full Stack | Phase I Lead",
    Photo: DavidTPic,
    Bio: "Hi y'all! My name is David Tang and I am a second-year CS major at UT Austin. I'm from Rowlett, Texas a suburb near Dallas. In my free time, I enjoy playing video games and listening to music, especially jazz and classical!",
    Commits: 0,
    Issues: 0,
    Tests: 0,
  },
  {
    Name: "Griffin Urban",
    Username: "griffinurbs",
    Email: "griffinurbs@utexas.edu",
    Role: "Full Stack | Phase IV Lead",
    Photo: GriffinUPic,
    Bio: "Hey! My name is Griffin Urban, a Junior at UT Majoring in CS with a minor in Slavic and Eurasian Languages. I’m from The Woodlands, which is just north of Houston, and I spend my free time learning languages, playing video games, and sometimes flying!",
    Commits: 2,
    Issues: 16,
    Tests: 21,
  },
];

const toolInfo = [
  {
    Logo: AWSLogo,
    Link: "https://aws.amazon.com/",
    Title: "Amazon Web Service",
    Desc: "Tool used for cloud hosting",
  },

  {
    Logo: EBLogo,
    Link: "https://aws.amazon.com/elasticbeanstalk/",
    Title: "Elastic Beanstalk",
    Desc: "Tool for backend deployment",
  },

  {
    Logo: AmazonRDSLogo,
    Link: "https://aws.amazon.com/rds/",
    Title: "Amazon RDS",
    Desc: "Tool for relational database hosting",
  },

  {
    Logo: ReactLogo,
    Link: "https://www.reactjs.org/",
    Title: "React",
    Desc: "JavaScript framework used for UI front-end web development",
  },

  {
    Logo: BootstrapLogo,
    Link: "https://react-bootstrap.github.io/",
    Title: "React Bootstrap",
    Desc: "JavaScript library used for UI foundation",
  },

  {
    Logo: MaterialLogo,
    Link: "https://mui.com/",
    Title: "Material UI",
    Desc: "JavaScript library used for UI foundation",
  },

  {
    Logo: PrettierLogo,
    Link: "https://prettier.io/",
    Title: "Prettier",
    Desc: "Code formatter",
  },

  {
    Logo: BlackLogo,
    Link: "https://pypi.org/project/black/",
    Title: "Black",
    Desc: "Tool for Python code formatting",
  },

  {
    Logo: NamecheapLogo,
    Link: "https://www.namecheap.com/",
    Title: "Namecheap",
    Desc: "Domain name registrar",
  },

  {
    Logo: GitLabLogo,
    Link: "https://www.gitlab.com",
    Title: "GitLab",
    Desc: "Git repository and CI/CD platform",
  },

  {
    Logo: PostmanLogo,
    Link: "https://www.postman.com/",
    Title: "Postman",
    Desc: "Tool used for design and testing of our API",
  },

  {
    Logo: DiscordLogo,
    Link: "https://www.discord.com/",
    Title: "Discord",
    Desc: "Tool for communication with team members",
  },

  {
    Logo: JestLogo,
    Link: "https://jestjs.io/",
    Title: "Jest",
    Desc: "Tool for JS frontend unit tests",
  },

  {
    Logo: SeleniumLogo,
    Link: "https://www.selenium.dev/",
    Title: "Selenium",
    Desc: "Tool used to test the frontend GUI",
  },

  {
    Logo: DockerLogo,
    Link: "https://www.docker.com/",
    Title: "Docker",
    Desc: "Tool used to dockerize dependencies",
  },

  {
    Logo: PostgresqlLogo,
    Link: "https://www.postgresql.org/",
    Title: "PostgreSQL",
    Desc: "Tool used for relational database",
  },

  {
    Logo: SQLalchemyLogo,
    Link: "https://flask-sqlalchemy.palletsprojects.com/en/2.x/",
    Title: "SQLAlchemy",
    Desc: "Tool used for object-relations and SQL access",
  },

  {
    Logo: FlaskLogo,
    Link: "https://flask.palletsprojects.com/en/2.1.x/",
    Title: "Flask",
    Desc: "Tool for API development",
  },

  {
    Logo: D3Logo,
    Link: "https://d3js.org/",
    Title: "D3",
    Desc: "JavaScript library for visualizations",
  },

  {
    Logo: RechartsLogo,
    Link: "https://recharts.org/en-US/",
    Title: "Recharts",
    Desc: "JavaScript library for visualizations",
  },
];

const apiInfo = [
  {
    Logo: EPALogo,
    Link: "https://www.epa.gov/enviro/envirofacts-data-service-api",
    Title: "US Environmental Protection Agency API",
    Desc: "API for greenhouse gas emissions",
  },

  {
    Logo: EIALogo,
    Link: "https://www.eia.gov/opendata/",
    Title: "US Energy Information Administration API",
    Desc: "API for fossil fuel and electricity usage",
  },

  {
    Logo: GMapsLogo,
    Link: "https://developers.google.com/maps",
    Title: "Google Maps API",
    Desc: "API for location info for facilities",
  },

  {
    Logo: GCivicsLogo,
    Link: "https://developers.google.com/civic-information",
    Title: "Google Civics API",
    Desc: "API for linking locations to political representatives",
  },

  {
    Logo: ClearBitLogo,
    Link: "https://dashboard.clearbit.com/docs#enrichment-api-company-api",
    Title: "ClearBit API",
    Desc: "API for company logos",
  },

  {
    Logo: OrbIntelLogo,
    Link: "https://api.orb-intelligence.com/docs/",
    Title: "Orb Intelligence API",
    Desc: "API for company information",
  },

  {
    Logo: GitLabLogo,
    Link: "https://www.gitlab.com",
    Title: "GitLab",
    Desc: "API for Git repository statistics",
  },
];

const repoAndAPI = [
  {
    Logo: GitLabLogo,
    Link: "https://gitlab.com/AlejandroCantu/group2",
  },
  {
    Logo: PostmanLogo,
    Link: "https://documenter.getpostman.com/view/19742333/UVksLuG4",
  },
];

export { memberInfo, toolInfo, apiInfo, repoAndAPI };
