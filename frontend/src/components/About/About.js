import React, { useEffect, useState } from "react";
import {
  Col,
  Card,
  Container,
  ListGroup,
  ListGroupItem,
  Table,
} from "react-bootstrap";
import { memberInfo, toolInfo, apiInfo, repoAndAPI } from "./AboutInfo.js";
import { Card as MatCard } from "@mui/material";
import {
  CardContent,
  CardMedia,
  Typography,
  CardActionArea,
  Stack,
} from "@mui/material";
import "../../styles/About.css";

const getGitLabStats = async () => {
  let commitCount = 0;
  let issueCount = 0;
  let testCount = 0;

  memberInfo.forEach((member) => {
    testCount += member.Tests;
  });

  let commits = [],
    page = [],
    pageNum = 1;
  do {
    page = await fetch(
      `https://gitlab.com/api/v4/projects/33832274/repository/commits?per_page=100&page=${pageNum}&all=true`
    );
    page = await page.json();
    commits = [...commits, ...page];
    pageNum += 1;
  } while (page.length === 100);
  commits.forEach((commit) => {
    const { author_name, author_email } = commit;
    memberInfo.forEach((member) => {
      if (
        member.Name === author_name ||
        member.Username === author_name ||
        member.Email === author_email
      ) {
        member.Commits += 1;
      }
    });
    commitCount += 1;
  });

  pageNum = 0;
  page = [];
  let issues = [];
  do {
    page = await fetch(
      `https://gitlab.com/api/v4/projects/33832274/issues?per_page=100&page=${pageNum}`
    );
    page = await page.json();
    issues = [...issues, ...page];
    pageNum += 1;
  } while (page.length === 100);

  issues.forEach((issue) => {
    const { author } = issue;
    const { name, username } = author;
    memberInfo.forEach((member) => {
      if (
        member.Name === name ||
        member.Username === username ||
        member.Name === username
      ) {
        member.Issues += 1;
      }
    });
    issueCount += 1;
  });

  return {
    teamMembers: memberInfo,
    numCommits: commitCount,
    numIssues: issueCount,
    numTests: testCount,
  };
};

const About = () => {
  const [teamMembers, setTeamMembers] = useState([]);
  const [numCommits, setNumCommits] = useState(0);
  const [numIssues, setNumIssues] = useState(0);
  const [numTests, setNumTests] = useState(0);

  useEffect(() => {
    const getData = async () => {
      if (teamMembers === undefined || teamMembers.length === 0) {
        const stats = await getGitLabStats();
        setTeamMembers(stats.teamMembers);
        setNumCommits(stats.numCommits);
        setNumIssues(stats.numIssues);
        setNumTests(stats.numTests);
      }
    };
    getData();
  }, [teamMembers]);

  return (
    <Container className="wrapper">
      <h1>About Us</h1>
      <Container>
        <p>
          FindingFootprints is a website that allows individuals to become
          informed on contributors to climate change in their local areas and
          across the United States. By informing people about companies and
          facilities damaging the environment, we hope to inspire the average
          person to take action and help prevent worsening climate change.
        </p>
      </Container>
      <h1 className="wrapper">Team Members</h1>
      <Stack direction="row" justifyContent="center" gap="32px">
        {teamMembers.map((member) => {
          return (
            <MatCard className="bioCard" style={{ width: "18rem" }}>
              <Card.Img className="bioPic" variant="top" src={member.Photo} />
              <CardContent>
                <Card.Title>{member.Name}</Card.Title>
                <Card.Subtitle>{member.Role}</Card.Subtitle>
                <Card.Text className="bioWrapper">{member.Bio}</Card.Text>
                <ListGroup variant="flush">
                  <ListGroupItem>Commits: {member.Commits}</ListGroupItem>
                  <ListGroupItem>Issues: {member.Issues}</ListGroupItem>
                  <ListGroupItem>Tests: {member.Tests}</ListGroupItem>
                </ListGroup>
              </CardContent>
            </MatCard>
          );
        })}
      </Stack>

      <h1>Repository Statistics</h1>
      <Container>
        <div className="repoStats">
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Total Commits</th>
                <th>Total Issues</th>
                <th>Total Tests</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{numCommits}</td>
                <td>{numIssues}</td>
                <td>{numTests}</td>
              </tr>
            </tbody>
          </Table>
        </div>
      </Container>

      <h1>Tools</h1>
      <Stack direction="row" justifyContent="center" flexWrap="wrap">
        {toolInfo.map((tool) => {
          return (
            <Col key={tool.Link} as="div">
              <MatCard className="infoCard" variant="outlined">
                <CardActionArea className="customAction" href={tool.Link}>
                  <CardMedia
                    className="logo"
                    component="img"
                    image={tool.Logo}
                  />
                  <CardContent>
                    <Typography variant="body1" color="text.primary">
                      {tool.Title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      {tool.Desc}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </MatCard>
            </Col>
          );
        })}
      </Stack>

      <h1>APIs Utilized</h1>
      <Stack direction="row" justifyContent="center" flexWrap="wrap">
        {apiInfo.map((api) => {
          return (
            <Col key={api.Link} as="div">
              <MatCard className="infoCard" variant="outlined">
                <CardActionArea
                  className="customAction"
                  href={api.Link}
                  target="_blank"
                >
                  <CardMedia
                    className="logo"
                    component="img"
                    image={api.Logo}
                  />
                  <CardContent>
                    <Typography variant="body1" color="text.primary">
                      {api.Title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      {api.Desc}
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </MatCard>
            </Col>
          );
        })}
      </Stack>

      <h1>GitLab Repository and Postman API</h1>
      <Stack direction="row" justifyContent="center" flexWrap="wrap">
        {repoAndAPI.map((tool) => {
          return (
            <Col key={tool.Link} as="div">
              <MatCard className="infoCard" variant="outlined">
                <CardActionArea
                  className="customAction"
                  href={tool.Link}
                  target="_blank"
                >
                  <CardMedia
                    className="GLPM"
                    component="img"
                    image={tool.Logo}
                  />
                </CardActionArea>
              </MatCard>
            </Col>
          );
        })}
      </Stack>
    </Container>
  );
};
export default About;
