import React, { useEffect, useState, useRef } from "react"
import * as d3 from "d3";
import LegendColor from "d3-svg-legend";
import { CircularProgress, Stack } from "@mui/material";

const requestOptions = {
    method: 'GET',
    redirect: 'follow',
};

export default function PoliticianActivityChart() {
    const svgRef = useRef();
    const [data, setData] = useState([]);
    const [loaded, setLoaded] = useState(false);

	const classFormatter = (party, chamber) => {
		party = party.charAt(0).toUpperCase() + party.slice(1);
		chamber = chamber.charAt(0).toUpperCase() + chamber.slice(1);
		return party + "-" + chamber;
	}

    useEffect (() => {
        async function fetchPoliticians()  {
			return await fetch('https://api.politistock.me/politicians', requestOptions);
        };
        fetchPoliticians()
			.then(response => {return response.json();})
			.then(json => {
				let pols = [];
				delete json["num_entries"];
				for (const item in json) {
					let elem = [];
					elem.push(json[item]["traded_volume"]);
					elem.push(json[item]["num_trades"]);
					elem.push(classFormatter(json[item]["party"], json[item]["chamber"]));
					elem.push(json[item]["id"]);
					pols.push(elem);
				}
				setData(pols);
			});
		setLoaded(true);
    }, []);

	const h = 300;
	const w = 700;
	const svg = d3.select(svgRef.current)
					.attr('width', w)
					.attr('height', h)
					.style('overflow', 'visible')
					.style('margin-top', '50px')
					.style('margin-bottom', '50px')

	svg.append("rect")
	.attr("x",0)
    .attr("y",0)
    .attr("height", h)
    .attr("width", w)
    .style("fill", "#bfbfbf")

	const xScale = d3.scaleLinear().domain([0, 500000000]).range([0, w]),
				yScale = d3.scaleLinear().domain([0, 12000]).range([h, 0]);
	
	// X label
	svg.append('text')
	.attr('x', w/2)
	.attr('y', h + 50)
	.attr('text-anchor', 'middle')
	.style('font-family', 'Helvetica')
	.style('font-size', 20)
	.text('Volume ($)');

	// Y label
	svg.append('text')
	.attr('x',  h/2)
	.attr('y', -125)
	.attr('text-anchor', 'middle')
	.attr('transform', 'translate(60,' + h + ')rotate(-90)')
	.style('font-family', 'Helvetica')
	.style('font-size', 20)
	.text('Trades');

	const xAxis = d3.axisBottom(xScale);
	const yAxis = d3.axisLeft(yScale);
	svg.append('g')
		.call(xAxis)
		.attr('transform', `translate(0, ${h})`);
	svg.append('g')
		.call(yAxis);

	svg.selectAll("mydots")
	
	svg.append("g")
		.attr("class", "legendOrdinal")
		.attr("transform", "translate(50,-50)")
	
	var color = d3.scaleOrdinal()
	.domain(["Republican-Senate", "Republican-House", "Democrat-Senate", "Democrat-House"])
	.range(["#b80606","#ff3300", "#0023b0", "#6383ff"])

	var legendOrdinal = LegendColor.legendColor()
		.scale(color)
		.orient("horizontal")
		.shapePadding(175);

	svg.select(".legendOrdinal")
  		.call(legendOrdinal);

	svg.append('g')
		.selectAll("dot")
		.data(data)
		.enter()
		.append('circle')
		.attr("cx", function (d) { return xScale(d[0]); } )
		.attr("cy", function (d) { return yScale(d[1]); } )
		.attr('r', 5.5)
		.style("fill", function(d) {return color(d[2]) } )
		.on("click", function (e, d) { click(d) } );

	const click = (d) => {
		let id = d[3];
		window.location.href = "https://www.politistock.me/politicians/" + id;
	} 

	if(!loaded){
		return <CircularProgress />
	}
    return (  
        <Stack className="mapPadding" direction="row" justifyContent="center" flexWrap="wrap" alignItems="center">
			<div className=".rect"/>
            <svg ref={svgRef}></svg>
        </Stack>
    );
}

