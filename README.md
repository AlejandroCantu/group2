# FindingFootprints

Welcome to the GitLab repository for FindingFootprints!<br>

**About**: FindingFootprints is a website that allows individuals to become informed on contributors to climate change in their local areas and across the United States. By informing people about companies and facilities damaging the environment, we hope to inspire the average person to take action and help prevent worsening climate change.<br>

## Group Members

| Name            | UT EID  | GitLab ID       |
| --------------- | ------- | --------------- |
| David Tang      | dwt572  | @davtang        |
| Alejandro Cantu | ac2728  | @AlejandroCantu |
| Griffin Urban   | gdu64   | @griffinurbs    |
| Vibhuti Sharma  | vs23369 | @vibhutisharma  |

The project leader for Phase 1 was David (@davtang).
The project leader for Phase 2 was Alejandro (@AlejandroCantu).
The project leader for Phase 3 was Vibhuti (@vibhutisharma).
The project leader for Phase 4 was Griffin (@griffinurbs).

## Links

Our website (prod): https://www.findingfootprints.me<br>
Develop website: https://develop.d2yu5jvosjyh52.amplifyapp.com/<br>
API endpoint: https://api.findingfootprints.me/<br>
Postman: https://documenter.getpostman.com/view/19742333/UVksLuG4<br>
GitLab Pipelines: https://gitlab.com/AlejandroCantu/group2/-/pipelines<br>
Youtube Presentation Link: https://www.youtube.com/watch?v=qDLs6qO0b6o

## Git SHA

Phase 1: 2ca7f987bdf81c2cf625d0de72b1acc5ed8c3509
Phase 2: 9ea3e48a27154fb4590c4b7b9f921c614dacfa26
Phase 3: 81b556bc9c392809e4acc80ed3d6adfc4c9e1d3e
Phase 4: a98363323af28577727b5ee59b3035ea37076482

## Completion Time

### Phase 1

| Name            | Estimated Hours | Actual Hours |
| --------------- | :-------------: | -----------: |
| David Tang      |       14        |           24 |
| Alejandro Cantu |       15        |           20 |
| Griffin Urban   |       16        |           18 |
| Vibhuti Sharma  |       15        |           24 |

### Phase 2
| Name            | Estimated Hours | Actual Hours |
| --------------- | :-------------: | -----------: |
| David Tang      |       25        |           42 |
| Alejandro Cantu |       24        |           28 |
| Griffin Urban   |       30        |           52 |
| Vibhuti Sharma  |       22        |           27 |

### Phase 3
| Name            | Estimated Hours | Actual Hours |
| --------------- | :-------------: | -----------: |
| David Tang      |       25        |           35 |
| Alejandro Cantu |       16        |           24 |
| Griffin Urban   |       15        |           20 |
| Vibhuti Sharma  |       20        |           24 |

### Phase 4
| Name            | Estimated Hours | Actual Hours |
| --------------- | :-------------: | -----------: |
| David Tang      |       30        |           42 |
| Alejandro Cantu |       28        |           30 |
| Griffin Urban   |       10        |           15 |
| Vibhuti Sharma  |       20        |           32 |

## Comments

README formatting, GitLab structure, front-end code adapted from [TexasVotes](https://gitlab.com/forbesye/fitsbits/-/blob/master/README.md), [TrailMix](https://gitlab.com/cs373-11am-group-6/trailmix), styling and material ui from [UniverCity](https://gitlab.com/coleweinman/swe-college-project/), unit tests structure from [MusicCity] (https://gitlab.com/m3263/musiccity), Grid layout for model pages from[SafeTravels] (https://gitlab.com/dferndz/cs-373-project)<br>
